import { shallowMount } from '@vue/test-utils'
import MenuWhere from '@/components/MenuWhere.vue'

describe('MenuWhere.vue Test', () => {
  it('renders basic data when created', () => {
    const wrapper = shallowMount(MenuWhere)
    expect(wrapper.vm.$options.name).toMatch('MenuWhere')
    expect(wrapper.findAll("h1").at(0).text()).toMatch("Hi, where am I?")
    expect(wrapper.findAll(".btnText").length).toEqual(3)
    expect(wrapper.findAll(".btnText").at(0).text()).toMatch("GDH")
    expect(wrapper.findAll(".btnText").at(1).text()).toMatch("WARD")
    expect(wrapper.findAll(".btnText").at(2).text()).toMatch("OTHER")
  })
})