import { shallowMount } from '@vue/test-utils'
import Header from '@/components/Header.vue'

describe('Header.vue Test', () => {
  it('renders basic data when created', () => {
    const wrapper = shallowMount(Header)
    expect(wrapper.vm.$options.name).toMatch('Header')
    expect(wrapper.findAll("#sound").exists()).toBeTruthy()
    expect(wrapper.findAll('#sound').at(0).text()).toMatch(/🔈 \d+\%$/g)
    expect(wrapper.findAll("#ip").exists()).toBeTruthy()
    expect(wrapper.findAll('#ip').at(0).text()).toMatch(/(\d+\.){3}\d+/g)
    expect(wrapper.findAll("#battery").exists()).toBeTruthy()
    expect(wrapper.findAll('#battery').at(0).text()).toMatch(/🔋 \d+\%/g)
  })
})