module.exports = {
  lintOnSave: true,
  publicPath: process.env.VUE_APP_PATH || '/apps/cgh-main/launcher/',
  outputDir: process.env.VUE_APP_BUILD_DIR || '../cgh-package/html/launcher'
}