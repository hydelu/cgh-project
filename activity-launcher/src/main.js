import Vue from 'vue'
import App from './App.vue'
import { store } from './js/store.js'
import { robot } from './js/robot.js'

Vue.config.productionTip = false

Vue.prototype.$store = store
Vue.prototype.robot = robot

var getUrlParam = function (parameter, defaultvalue, url) {
  if (url == undefined) {
    url = window.location.href
  }
  var urlparameter = defaultvalue;
  if(url.indexOf(parameter) > -1) {
    var vars = {}
    url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
    });
    urlparameter = vars[parameter];
  }
  return urlparameter;
}

Vue.mixin({
  methods: {
    getUrlParam: getUrlParam
  }
})

new Vue({
  data: store.state,
  render: h => h(App)
}).$mount('#app')
