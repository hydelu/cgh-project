import Vue from 'vue'

export const store = {
  state: Vue.observable({
    wifiIp: "192.168.11.2",
    batteryLevel: 50,
    soundLevel: 90,
    currentStep: "start",
    currentApp: "",
    language: 'English',
    roleplaySettings: {
      scenarioName: "",
      scenarioDisplay: ""
    },
    visit: "",
    analytics: {
      location: "",
      workflow: ""
    }
  }),
<<<<<<< Updated upstream
  possibleStates: [
    "start", "what", "language", "appChoice", "orientation", "soundTherapy", "exercises",
    "roleplay", "roleplayChoice", "appRunning", "themeSongChoice", "themeSong","quiz",
    "memoryGame","categories", "similaritiesGame","analytics"],
=======
  possibleStates: ["start", "what", "language", "appChoice", "orientation", "soundTherapy", "exercises", "roleplay", "appRunning", "themeSongChoice", "themeSong", "analytics"],
>>>>>>> Stashed changes
  previousState: {
    what: "start",
    language: "what",
    appChoice: "language",
    orientation: "appChoice",
    soundTherapy: "appChoice",
    exercises: "appChoice",
    roleplay: "appChoice",
<<<<<<< Updated upstream
    roleplayChoice: "roleplay",
=======
    analytics: "appChoice",
>>>>>>> Stashed changes
    themeSongChoice: "appChoice",
    themeSong: "themeSongChoice",
    quiz: "appChoice",
    memoryGame: "appChoice",
    categories: "appChoice",
    similaritiesGame:"appChoice",
    analytics: "appChoice",
  },
  setLocation(location) {
    this.state.analytics.location = location
  },
  setState(state) {
    if (this.possibleStates.indexOf(state) != -1)
      this.state.currentStep = state
    else
      throw 'impossible state: ' + state
  },
  setApp(app) {
    this.state.currentApp = app
  },
  setWorkflow(workflow) {
    this.state.analytics.workflow = workflow
  },
  setLanguage(language) {
    this.state.language = language
  },
  replaceState (stateString) {
    let newState = JSON.parse(stateString)
    this.setLocation(newState.analytics.location)
    this.setWorkflow(newState.analytics.workflow)
    this.setLanguage(newState.language)
    this.setApp(newState.currentApp)
    this.setState(newState.currentStep)
    if (newState.roleplaySettings != undefined)
      this.setScenario(newState.roleplaySettings.scenarioName, newState.roleplaySettings.scenarioDisplay)
  },
  getStateAsString () {
    return JSON.stringify({
      currentStep: this.state.currentStep,
      currentApp: this.state.currentApp,
      language: this.state.language,
      analytics: {
        location: this.state.analytics.location,
        workflow: this.state.analytics.workflow,
      },
      roleplaySettings: {
        scenarioName: this.state.roleplaySettings.scenarioName,
        scenarioDisplay: this.state.roleplaySettings.scenarioDisplay
      }
    })
  },
  setScenario(name, display) {
    this.state.roleplaySettings.scenarioName = name
    this.state.roleplaySettings.scenarioDisplay = display
  }
}