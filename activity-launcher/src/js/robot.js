export const robot = {
  session: null,
  isConnected: false,
  debugLog (msg) {
    if (robot.debug) {
      console.log(msg)
    }
  },
  connect (ip, onSuccess, onFail) {
    if (ip != undefined) {
      this.session = new QiSession(ip)
    } else {
      this.session = new QiSession()
    }
    var ref = this
    this.session.socket().on('connect', function () {
      robot.debugLog("Qimessaging: connected!")
      ref.isConnected = true
      if (onSuccess != undefined) {
        setTimeout(onSuccess, 100)
      }
    })
    this.session.socket().on('disconnect', function() {
      robot.debugLog('Qimessaging: disconnected!');
      ref.isConnected = false
      if (onFail != undefined) {
        setTimeout(onFail, 100)
      }
    });
  },
  getService (serviceName) {
    return this.session.service(serviceName)
  },
  call (serviceName, functionName, callback, errorCallback, ...params) {
    this.getService(serviceName).done(function (service) {
      //console.log("qicli call: " + serviceName + "." + functionName)
      try {
        service[functionName](...params).done(callback).fail(errorCallback)
      } catch (e) {
        errorCallback(e)
      }
    })
  },
  setTabletState(state) {
    this.call("CGHTemplateController", "set_state", null, null, state)
  },
  setLanguage (lang, callback) {
    this.call("ALTextToSpeech", "setLanguage", () => {
      this.call("ALSpeechRecognition", "setLanguage", callback, null, lang)
    }, null, lang)
  },
  startActivity(template, config) {
    this.call("CGHTemplateController", "start_template", null, null, template, config)
  },
  subscribeToTabletCommands(callback) {
    if (!this.isConnected) {
      setTimeout(() => { this.subscribeToTabletCommands(callback) }, 500)
    } else {
      this.subscribeToSignal("CGHTemplateController", "showOnTablet", callback)
    }
  },
  subscribeToSignal(serviceName, signalName, callback) {
    this.getService(serviceName).done(function (service) {
      try {
        service[signalName].connect(callback)
      } catch (e) {
        console.error(e)
      }
    })
  }
}