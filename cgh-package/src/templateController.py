# -*- coding: utf-8 -*-

from slidePlayer import SlidePlayer
from slideList import SlideList
import qi
import json
import logging
import os
import traceback
import random
import time
import datetime

SERVICE_NAME = "CGHTemplateController"
PACKAGE_ID = "cgh-main"
MOVE_DISTANCE = 0.15 # distance to move with remote in meters
MOVE_ANGLE = 0.4 # angle to rotate with remote in radiants [-3.14, 3.14]


logging.basicConfig(
  filename=qi.path.userWritableDataPath("CGHLogs", "CGHTemplate.log"),
  level=logging.DEBUG,
  format='%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s',
  datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(SERVICE_NAME)
logger.info("STARTING SCRIPT")


class TemplateController(object):
  """demo of json parser for TemplateController"""

  @qi.nobind
  def __init__(self, session):
    try:
      self.session = session
      self.state = "not_running"
      # configuring source folder, will change when using editor
      self.data_folder = qi.path.userWritableDataPath("CGHTemplate", "")
      self.exercise_n_loop = 3
      self.images_folder = "app_images"
      # subscribe to touch events
      gestures = None
      while gestures == None:
        # waiting for ALTactileGesture before starting
        try:
          gestures = self.session.service("ALTactileGesture")
        except RuntimeError:
          time.sleep(1)
      self.FrontMiddleTap = gestures.createGesture(['000', '110', '000'])
      logger.info(self.FrontMiddleTap)
      gestures.onGesture.connect(self.on_gesture)
      # BackBumperPressed
      memory = self.session.service("ALMemory")
      self.bumperSubscriber = memory.subscriber("BackBumperPressed")
      self.bumperSubscriber.signal.connect(self.on_bumper_pressed)
      # language
      self.language = None
      while self.language == None:
        try:
          self.language = self.session.service(
            "ALTextToSpeech").getLanguage()
        except RuntimeError:
          time.sleep(1)
      # create tablet API signal
      self.showOnTablet = qi.Signal("(s)")
      self.tabletState = qi.Signal("(s)")
      self.appFinished = qi.Signal("()")
      self.current_tablet_state = {
        "currentStep": "start",
        "currentApp": "",
        "language": "English",
        "analytics": {
          "location": "",
          "workflow": ""
        }
      }
      self.current_template = None
      print self.current_tablet_state
      logger.info("started")
    except Exception as err:
      logger.error(str(err))

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String])
  def start_template(self, template_name, config_name):
    """starts a template only if no template is currently running"""
    if self.state != "not_running":
      return
    
    # initialize template
    if template_name == "roleplay":
      logger.info(config_name)
      self.setup_roleplay(config_name)
    elif template_name == "reality orientation":
      self.setup_reality_orientation()
    elif template_name == "exercises":
      logger.info(config_name)
      self.setup_exercises(config_name, None)
    elif template_name == "sound therapy":
      logger.info(config_name)
      self.setup_sound_therapy(config_name)
    elif template_name == "quiz":
      logger.info(config_name)
      self.setup_quiz(config_name)
    elif template_name== 'similaritiesGame':
      logger.info(config_name)
      self.setup_similaritiesGame(config_name)
    else:
      return
    self.current_template = template_name

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String])
  def start_roleplay(self, template_name, preset_id):
    """starts roleplay specifically"""
    if self.state != "not_running":
      return
    logger.info(template_name)
    if template_name == "scenario1":
      template_path = "./data/roleplay/" + self.language + "/roleplay.json"
    else:
      template_path = "./data/roleplay/" + self.language + "/roleplay2.json"
    self.setup_roleplay(template_path, preset_id=preset_id, template_type=template_name)
    self.current_template = "roleplay"

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.List(qi.String)])
  def start_exercise(self, template_name, config_name, music_list):
    """starts a template specifically an exercise"""
    self.setup_exercises(config_name, music_list)

  @qi.bind(qi.Void)
  def stop_template(self):
    """stops the current template"""
    if self.state != "not_running":
      logger.info("stopping template")
      self.slide_player.stop()

  @qi.bind(qi.String)
  def getState(self):
    """DEPRECATED, use get_state instead"""
    return self.state

  @qi.bind(qi.String)
  def getRobotIp(self):
    services = self.session.service("ALConnectionManager").services()
    for service in services:
      network = dict(service)
      if network.get("Type") == "wifi":
        details = dict(network.get("IPv4"))
        return details.get("Address", "")
    return ""

  @qi.bind(qi.Void, paramsType=[qi.String])
  def set_language(self, language):
    available_languages = ["English", "Chinese", "Tamil", "Malay"]
    if language in available_languages:
      self.language = language
      if language in ["English", "Chinese"]:
        self.session.service("ALTextToSpeech").setLanguage(language)

  @qi.bind(qi.Void, paramsType=[qi.String])
  def set_state(self, state):
    """This information will be relayed to robot tablet and all remote controller"""
    self.current_tablet_state = json.loads(state)
    self.tabletState(state)

  @qi.bind(qi.String)
  def get_state(self):
    return json.dumps(self.current_tablet_state)

  @qi.bind(qi.Void, paramsType=[qi.String])
  def exercise_say(self, key):
    """offers localize way to count or say left/right during anims"""
    # goal is to do: key_to_sentences.get(current_language).get(key)
    key_to_sentences = {
      "English": {
        "up": "up",
        "down": "down",
        "front": "front",
        "back": "back",
        "side": "side",
        "open": "open",
        "close": "close",
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "left": "left",
        "right": "right"
      },
      "Chinese": {
        "up": "上",
        "down": "下",
        "front": "前",
        "back": "后",
        "side": "旁边",
        "open": "开",
        "close": "关",
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "left": "剩下",
        "right": "对"
      }
    }
    text = key_to_sentences.get(self.language, {}).get(key, key)
    self.session.service("ALTextToSpeech").say(text)

  @qi.bind(qi.Int32)
  def get_exercise_loop_number(self):
    return self.exercise_n_loop

  @qi.bind(qi.Void)
  def pause_template(self):
    self.state = "paused"

  @qi.bind(qi.Void)
  def resume_template(self):
    self.state = "running"
    self.slide_player.resume()

  @qi.bind(qi.Void)
  def skip_forward(self):
    logger.info("skip_forward")
    if self.state != "not_running":
      self.slide_player.skip_loop()

  @qi.bind(qi.Void)
  def skip_backward(self):
    logger.info("skip_backward")
    if self.state != "not_running":
      self.slide_player.replay_loop()

  @qi.bind(qi.Void, paramsType=[qi.String])
  def start_categories(self, preset_id):
    """starts a categories game based on the preset chosen"""
    # create relevant paths
    template_path = "./data/categories/" + self.language + "/template.json"
    self.current_template = "categories"
    preset_folder = self.data_folder + "/categories/" + self.language + "/presets/" + preset_id
    locations_folder = self.data_folder + "/categories/" + self.language + "/locations"
    # loading main slides data
    self.data = self.load_json_from_path(template_path)
    logger.info("data file loaded")
    self.slides = SlideList(self.data["slides"])
    # loading parameters, setup sets lists
    self.parameters = self.load_json_from_path(preset_folder)
    all_items, locations = self.load_all_categories_items()
    for location_set in self.parameters["catsList"]:
      loc_id = location_set["location"]
      location_set["location_id"] = loc_id
      location_set["locItems"] = self.create_item_set(
        loc_id, all_items, location_set["number_items"])
      location_set["location"] = locations[loc_id]["text"]
      location_set["location_image"] = locations[loc_id]["image"]
    # setup slide player
    self.slide_player = SlidePlayer(
      self.slides,
      self,
      images_folder="./data/categories/images",
      speech_folder="./data/categories/" + self.language,
      parameters=self.parameters,
      finished_callback=self.on_finished_template)
    logger.info("starting categories")
    self.slide_player.start()

  @qi.bind(qi.Void, paramsType=[qi.String])
  def start_memory_game(self, preset_id):
    """starts a memory game based on the preset chosen"""
    try:
      # create relevant paths
      template_path = "./data/memoryGame/" + self.language + "/template.json"
      self.current_template = "memoryGame"
      preset_folder = self.data_folder + "/memoryGame/" + self.language + "/presets/" + preset_id
      categories_folder = self.data_folder + "/memoryGame/" + self.language + "/categories/"
      # loading main slides data
      self.data = self.load_json_from_path(template_path)
      self.slides = SlideList(self.data["slides"])
      # loading parameters, setting up sets
      self.parameters = self.load_json_from_path(preset_folder)
      self.parameters["setsList"] = []
      for item_sets in self.parameters["sets"]:
        # gather all items in 1 list, shuffle and keep only X number
        all_items = []
        for category in item_sets["categories"]:
          cat_data = self.load_json_from_path(categories_folder + category + "/data.json")
          all_items += cat_data["items"]
        logger.info(all_items)
        random.shuffle(all_items)
        keep_items = all_items[:item_sets["items"]]
        logger.info(keep_items)
        self.parameters["setsList"].append(keep_items)
      # set up slide player
      self.slide_player = SlidePlayer(
        self.slides,
        self,
        images_folder="./data/memoryGame/images",
        speech_folder="./data/memoryGame/" + self.language,
        parameters=self.parameters,
        finished_callback=self.on_finished_template)
      self.state = "running"
      logger.info("starting memory game")
      self.slide_player.start()
    except Exception as err:
      print err

  @qi.nobind
  def setup_roleplay(self, config_path, preset_id=None, template_type=None):
    """loads config and defines folders such as sounds and images"""
    preset_path = self.data_folder + "/roleplay/" + template_type + "/" + self.language + "/" + preset_id
    # load main data
    self.data = self.load_json_from_path(config_path)
    self.slides = SlideList(self.data["slides"])
    # load parameters, create calculated params, convert integers to strings
    self.parameters = self.load_json_from_path(preset_path + "/data.json")
    if template_type == "scenario1":
      self.parameters["change_amount"] = self.parameters["wallet_amount"] - self.parameters["second_item_cost"]
      for param in ["change_amount", "wallet_amount", "first_item_cost", "second_item_cost"]:
        self.parameters[param] = str(self.parameters[param])
    elif template_type == "scenario2":
      self.parameters["cost_two_way"] = str(int(self.parameters["cost_one_way"])*2)
    self.slide_player = SlidePlayer(
      self.slides,
      self,
      images_folder="./data/roleplay/images",
      sounds_folder="./data/roleplay/sounds",
      speech_folder="./data/roleplay/" + self.language,
      parameters=self.parameters,
      parameter_images=preset_path,
      finished_callback=self.on_finished_template)
    self.state = "running"
    logger.info("starting")
    self.slide_player.start()

  @qi.nobind
  def setup_reality_orientation(self):
    """loads template, configs, setups folders..."""
    try:
      logger.info("language: " + self.language)
      template_path = "./data/realityOrientation/" + \
        self.language + "/reality_orientation.json"
      config_path = self.data_folder + "/realityOrientation/data.json"
      self.data = self.load_json_from_path(template_path)
      self.slides = SlideList(self.data["slides"])
      config_data = self.load_json_from_path(config_path)
      self.parameters = config_data.get(self.language, {})
      month = datetime.datetime.now().month
      self.parameters["prime_minister_image"] = config_data.get("images")[
          0]
      self.parameters["month_description_image"] = config_data.get("images")[
          month]
      if self.language in ["English", "Chinese"]:
        self.parameters["month_description_text"] = self.parameters.get("months_comments")[
          month]
      else:
        lang = self.language.lower()
        self.parameters["this_month"] = "ro_{}_month_{}.ogg".format(lang, month)
        month_comments = self.parameters.get("months_comments")[month]
        n_sentence = random.randrange(len(month_comments.split(";")))
        self.parameters["month_description"] = "ro_{}_month_desc_{}_{}.ogg".format(lang, month, n_sentence)
      logger.info(self.parameters)
      parameter_images_folder = self.data_folder + "/realityOrientation/images"
      images_folder = "./data/realityOrientation/images"
      self.slide_player = SlidePlayer(
        self.slides,
        self,
        parameters=self.parameters,
        images_folder=images_folder,
        parameter_images=parameter_images_folder,
        finished_callback=self.on_finished_template,
        speech_folder="./data/realityOrientation/" + self.language)
      self.state = "running"
      self.slide_player.start()
    except Exception as err:
      logger.error(str(err))

  @qi.nobind
  def setup_exercises(self, preset, music):
    """loads template, preset, setups folders and music, etc..."""
    try:
      template_path = "./data/exercises/" + self.language + "/exercises.json"
      config_path = self.data_folder + "/exercises/presets/" + preset
      self.data = self.load_json_from_path(template_path)
      self.slides = SlideList(self.data["slides"])
      self.parameters = self.load_json_from_path(config_path)
      if music is not None:
        self.parameters["music_list"] = music
      self.slide_player = SlidePlayer(
        self.slides,
        self,
        parameters=self.parameters,
        finished_callback=self.on_finished_template,
        images_folder="./data/exercises/images",
        speech_folder="./data/exercises/" + self.language,
        sounds_folder=self.data_folder + "/exercises/music/")
      self.state = "running"
      self.slide_player.start()
      # TODO: the music from a list of songs
    except Exception as err:
      logger.error(str(err))

  @qi.nobind
  def setup_sound_therapy(self, preset):
    """loads template, preset, etc..."""
    try:
      template_path = "./data/soundTherapy/" + self.language + "/sound_therapy.json"
      config_path = self.data_folder + "/soundTherapy/" + \
        self.language + "/presets/" + preset
      self.data = self.load_json_from_path(template_path)
      self.slides = SlideList(self.data["slides"])
      self.parameters = self.load_json_from_path(config_path)
      self.parameters["soundsList"] = []
      for sound in self.parameters.get("sounds"):
        if sound.get("type") == "default":
          path = "./data/soundTherapy/sounds/" + \
            sound.get("sound_id") + "/data.json"
        else:
          path = self.data_folder + "/soundTherapy/" + self.language + \
            "/sounds/" + sound.get("sound_id") + "/data.json"
        sound_data = self.load_json_from_path(path)
        self.parameters["soundsList"].append(sound_data)
      random.shuffle(self.parameters["soundsList"])
      self.slide_player = SlidePlayer(
        self.slides,
        self,
        parameters=self.parameters,
        images_folder="./data/soundTherapy/images",
        speech_folder="./data/soundTherapy/" + self.language,
        finished_callback=self.on_finished_template)
      self.state = "running"
      self.slide_player.start()
    except Exception as err:
      logger.error(str(err))

  @qi.bind(qi.Void, paramsType=[qi.String])
  def move(self, direction):
    """move if not in exercise app"""
    logger.info("move: " + direction)
    if self.state == "running" and self.current_template == "exercises":
      return
    motion = self.session.service("ALMotion")
    if direction == "front":
      motion.moveTo(MOVE_DISTANCE, 0, 0)
    if direction == "back":
      motion.moveTo(-MOVE_DISTANCE, 0, 0)
    if direction == "strafe_left":
      motion.moveTo(0, MOVE_DISTANCE, 0)
    if direction == "strafe_right":
      motion.moveTo(0, -MOVE_DISTANCE, 0)
    if direction == "rotate_left":
      motion.moveTo(0, 0, MOVE_ANGLE)
    if direction == "rotate_right":
      motion.moveTo(0, 0, -MOVE_ANGLE)

  @qi.nobind
  def load_template(self, template_name):
    if template_name not in self.template_name_to_path.keys():
      raise AttributeError("invalide template_name: " + template_name)
    template_file_path = self.template_name_to_path.get(template_name)
    return self.load_json_from_path(template_file_path)

  @qi.nobind
  def load_json_from_path(self, template_path):
    with open(template_path) as f:
      data = json.load(f)
    return data

  @qi.nobind
  def on_gesture(self, gesture_name):
    logger.info("on_gesture: " + gesture_name)
    motions = ["SingleTap", "SingleFront",
           "SingleMiddle", self.FrontMiddleTap]
    if gesture_name in motions:
      self.pauseToggle()

  @qi.nobind
  def on_bumper_pressed(self, value):
    if value == 1:
      logger.info("bumper pressed")
      self.stop_template()

  @qi.nobind
  def pauseToggle(self):
    if self.state == "paused":
      self.resume_template()
    elif self.state == "running":
      self.pause_template()

  @qi.nobind
  def on_finished_template(self):
    logger.info("finished!")
    self.session.service("ALAudioPlayer").stopAll()
    self.state = "not_running"
    self.appFinished()

  @qi.nobind
  def _load_application(self):
    tablet = self.session.service("ALTabletService")
    url = "http://" + tablet.robotIp() + "/apps/" + PACKAGE_ID + "/launcher/"
    tablet.loadUrl(url)
    tablet.showWebview()

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String])
  def setup_theme_song(self, preset, visit):
    try:
      logger.info("theme song")
      logger.info(preset)

      if visit == "firstVisit":
        template_path = "./data/themeSong/" + self.language + "/themeSong1.json"
      else:
        template_path = "./data/themeSong/" + self.language + "/themeSong2.json"
      config_path = self.data_folder + "/themeSong/songs/" + preset+"/data.json"
      sound_path = self.data_folder + "/themeSong/songs/" + preset
      self.data = self.load_json_from_path(template_path)
      self.slides = SlideList(self.data["slides"])
      self.parameters = self.load_json_from_path(config_path)
      self.slide_player = SlidePlayer(
        self.slides,
        self,
        parameters=self.parameters,
        images_folder="./data/themeSong/images",
        sounds_folder=sound_path,
        speech_folder="./data/themeSong/" + self.language,
        finished_callback=self.on_finished_template)
      self.state = "running"
      self.slide_player.start()
    except Exception as err:
      logger.error(str(err))

  @qi.bind(qi.Void, paramsType=[qi.String])
  def preview_theme_song(self, preset):
    try:
      config_path = self.data_folder + "/themeSong/songs/" + preset+"/data.json"
      data = self.load_json_from_path(config_path)
      self.slide_player = SlidePlayer(
        [],
        self,
        parameters=data,
        sounds_folder=self.data_folder + "/themeSong/songs/" + preset,
      )
      src = data.get("sound_src")
      qi.async(self.slide_player._play_sound, src)
    except Exception as err:
      logger.error(str(err))

  @qi.bind(qi.Void)
  def stop_theme_song(self):
    try:
      self.slide_player = SlidePlayer(
        [],
        self,
      )
      self.slide_player._stop_music()
    except Exception as err:
      logger.error(str(err))
  
  @qi.bind(qi.Void)
  def setup_quiz(self,preset):
    try:
      template_path = "./data/quiz/"+self.language+"/quiz.json"  
      config_path = self.data_folder+"/quiz/"+self.language+\
      "/presets/"+preset  
      self.data = self.load_json_from_path(template_path)
      self.slides = SlideList(self.data["slides"])
      self.parameters = self.load_json_from_path(config_path)
      # logger.info(self.parameters)
      self.parameters["quizList"] = []
      for questions in self.parameters.get("questions"):
        if questions.get("type") == "default":
          path = "./data/quiz/questions/" + \
            questions.get("question_id") + "/data.json"
        else:
          path = self.data_folder + "/quiz/" + self.language + \
            "/questions/" + questions.get("question_id") + "/data.json"
        question_data = self.load_json_from_path(path)
        # logger.info(question_data)
        self.parameters["quizList"].append(question_data)
      random.shuffle(self.parameters["quizList"])
      self.slide_player = SlidePlayer(
        self.slides,
        self,
        parameters=self.parameters,
        images_folder="./data/quiz/images",
        speech_folder="./data/quiz/" + self.language,
        current_app="quiz",
        finished_callback=self.on_finished_template)
      self.state = "running"
      self.slide_player.start()
    except Exception as err:
      logger.error(str(err))
  
  def setup_similaritiesGame(self,preset):
    try:
      template_path = "./data/similarityGame/"+self.language+"/similarities.json"  
      config_path = self.data_folder+"/similarities/"+self.language+\
      "/presets/"+preset 
      self.data = self.load_json_from_path(template_path)
      self.slides = SlideList(self.data["slides"])
      self.parameters = self.load_json_from_path(config_path)
      self.parameters["sequenceList"] = []
      for sequence in self.parameters.get("sequence"):
        if sequence.get("type") == "default":
          path = "./data/similarityGame/sequences/" + \
              sequence.get("sequence_id") + "/data.json"
        else:
          path = self.data_folder + "/similarities/" + self.language + \
            "/sequences/" + sequence.get("sequence_id") + "/data.json"
        sequence_data=self.load_json_from_path(path)
        self.parameters['sequenceList'].append(sequence_data)
      random.shuffle(self.parameters['sequenceList'])
      self.slide_player = SlidePlayer(
        self.slides,
        self,
        parameters=self.parameters,
        images_folder="./data/similarityGame/images",
        speech_folder="./data/similarityGame/" + self.language,
        current_app="similarityGame",
        finished_callback=self.on_finished_template)
      self.state = "running"
      self.slide_player.start()
    except Exception as err:
      logger.error(str(err))

  @qi.nobind
  def load_all_categories_items(self):
    """
    creates a list of all possible items,
    each item contains item_text, item_image, location_text, location_id
    """
    all_data = []
    all_locations = {}
    folder_path = self.data_folder + "/categories/" + self.language + "/locations"
    try:
      locations = next(os.walk(folder_path), (None,None,None))[1]
      for loc in locations:
        try:
          data = None
          with open(folder_path + "/" + loc + "/data.json") as f:
            data  = json.load(f)
          location_text = data.get("location_text")
          location_image = loc + "/" + data.get("location_image")
          all_locations[loc] = {
            "text": location_text,
            "image": location_image
          }
          for item in data.get("items"):
            item["location_text"] = location_text
            item["item_image"] = loc + "/" + item["item_image"]
            item["location_id"] = loc
            all_data.append(item)
        except Exception as err:
          logger.warn(err)
          continue
    except Exception as err:
      logger.warn(str(err))
    return all_data, all_locations

  @qi.nobind
  def create_item_set(self, location, all_items, number_items):
    """
    Selects random X items from location, then number_items - X from other locations
    """
    n_good_items = random.randrange(1, number_items)
    good_items = [i for i in all_items if i["location_id"] == location]
    random.shuffle(good_items)
    chosen_items = good_items[:n_good_items]
    n_bad_items = number_items - len(chosen_items)
    bad_items = [i for i in all_items if i["location_id"] != location]
    random.shuffle(bad_items)
    chosen_items += bad_items[:n_bad_items]
    random.shuffle(chosen_items)
    return chosen_items
  
  @qi.bind(qi.Void, paramsType=[qi.String, qi.String])
  def theme_song_intro(self,language,visit):
    logger.info("theme song introduction")
    if visit=="firstVisit":
      intro ="intro1.json"
    else:
      intro ="intro2.json"
    path = "./data/themeSong/"+language
    data =self.load_json_from_path(path+"/"+intro)
    logger.info(str(data))

    self.slide_player = SlidePlayer(data.get("slides"),self,speech_folder=path)
    for slide in data.get("slides"):
      self.slide_player._anim_sound(slide)
      time.sleep(2)

def main():
  import sys
  app = qi.Application(sys.argv)
  app.start()
  if "--testing" in sys.argv:
    global TESTING
    TESTING = True
  controller = TemplateController(app.session)
  app.session.registerService(SERVICE_NAME, controller)
  app.run()


if __name__ == '__main__':
  main()