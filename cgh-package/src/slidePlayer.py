# -*- coding: utf-8 -*-

import os
import qi
import base64
import logging
import re
import json
import random
import time

from paramHandler import ParamHandler
from slideList import SlideList

logging.basicConfig(
  filename=qi.path.userWritableDataPath("CGHLogs", "CGHTemplate.log"),
  level=logging.DEBUG,
  format='%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s',
  datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)


class SlidePlayer(object):

  def __init__(self, slide_list, module, parameters=None, images_folder=None,
         sounds_folder=None, parameter_images=None, finished_callback=None, speech_folder=None, current_app=None):
    logger.info("new slide player is being created")
    # this is a SlideList
    self.slides = slide_list
    self.parameters = parameters
    self.images_folder = images_folder
    self.sounds_folder = sounds_folder
    self.speech_folder = speech_folder
    self.parameter_images = parameter_images
    self.current_app = current_app
    self.param_handler = ParamHandler()
    # root TemplateController
    self.module = module
    self.on_finished_cb = finished_callback
    self.is_stopping = False
    # background image loop
    self.bgimage_stop = False
    self.bgimage_index = -1
    self.bgimage_path = None
    self.bgimage_images = None
    self.bgimage_task = None
    # looping variables
    self.in_loop = False
    self.loop_player = None
    self.skip_transition = False # flag to signal skipping of 1 transition when skipping forward
    self.back_loop = False # flag to signal to replay the current loop when replaying back
    # current activity info
    self.current_animation = None
    
    # other
    # self.current_volume= self._get_current_volume()
    # logger.info("current_volume:"+str(self.current_volume))
    if os.path.exists("/home/nao"):
      self.pathToPackage = "/home/nao/.local/share/PackageManager/apps/cgh-main/"
    else:
      self.pathToPackage = os.getcwd() + "/"

  def start(self):
    logger.info("starting slide player")
    qi.async(self.next_slide)

  def stop(self):
    self.is_stopping = True
    self.music_must_stop = True
    if self.in_loop:
      self.sub_player.stop()
    else:
      if self.module.state == "paused":
        self.resume()
    s = self.module.session
    try:
      if self.current_animation is not None:
        s.service("ALBehaviorManager").stopBehavior(
          self.current_animation)
    except RuntimeError:
      pass
    try:
      s.service("ALTextToSpeech").stopAll()
    except RuntimeError:
      pass
    try:
      self._stop_music()
    except RuntimeError:
      pass

  def next_slide(self):
    if self.slides.next_slide() is None or self.is_stopping:
      self.on_finished()
      return
    if self.module.state != "paused":
      try:
        self.process_slide()
      except Exception as err:
        # catching all errors happening in the thread and printing them to log
        logger.info(str(err))
        traceback.print_exc()
        # finishing current template to let restart if needed
        self.on_finished()

  def register_finished_cb(self, finished_cb):
    self.on_finished_cb = finished_cb

  def process_slide(self):
    if self.slides.get_current_slide() is None or self.is_stopping:
      self.on_finished()
      return
    slide = self.slides.get_current_slide()
    logger.info(slide)
    if slide["type"] == "tablet":
      self.bgimage_stop = True
      if self.bgimage_task is not None:
        self.bgimage_task.stop()
      self._show_image(slide)
    elif slide["type"] == "animSay":
      text = slide["text"]
      if slide.get("random", False) == True:
        text = random.choice(text)
      self._animSay(slide["text"])
    elif slide["type"] == "animation":
      self._run_animation(slide["src"])

    elif slide["type"] == "pause":
      if slide["duration"] == 0:
        self.module.state = "paused"
        self.slides.next_slide()
      else:
        if slide.get("duration")=="<sequence_pause>":
          duration =int(self.replace_param(slide.get("duration")))
          duration = duration *1000000
        else:
          duration = int(slide["duration"] * 1000000)
        qi.async(self.next_slide, delay=duration)
      return
    elif slide["type"] == "sound":
      self._play_sound(slide["src"])
    elif slide["type"] == "hide_image":
      self.module.session.service("ALTabletService").hideImage()
    elif slide["type"] == "soundLoop":
      self._sound_loop(slide)
    elif slide["type"] == "loop":
      self._init_loop(slide)
      return
    elif slide["type"] == "backgroundMusic":
      self._background_music()
    elif slide["type"] == "stopMusic":
      self._stop_music()
    elif slide["type"] == "exerciseLoop":
      self._exercise_loop(slide)
    elif slide["type"] == "song":
      self._play_song(slide["src"])
    elif slide["type"] == "gameSlideshow":
      self._game_slideshow(slide)
    elif slide["type"] == "animSound":
      self._anim_sound(slide)
    qi.async(self.next_slide)

  def _show_image(self, slide, path=None):
    data = {}
    logger.info("image slide: " + str(slide))
    if slide.get("tablet_type") == "image":
      data["type"] = "image"
      src_data = slide.get("src", "")
      src = self.replace_param(src_data)
      logger.info("image slide: " + str(slide))
      if src != "":
        if src == src_data:
          if slide.get("path"):
            path= self.pathToPackage+ slide.get("path") + src
          else:  
            path = path or self.pathToPackage + self.images_folder + "/" + src
        else:
          # then it was taken from parameters, use that folder
          path = path or self.parameter_images + "/" + src
        pic64 = "data:image/png;base64, " + \
          base64.b64encode(open(path).read())
        data["image"] = pic64
      else:
        data["image"] = ""
    elif slide.get("tablet_type") == "images":
      self.bgimage_stop = False
      src_data = slide.get("src", "")
      self.bgimage_images = self.replace_param(src_data, change_type="list")
      if self.bgimage_images != "":
        if self.bgimage_images == src_data:
          self.bgimage_path = self.pathToPackage + self.images_folder + "/"
        else:
          # then it was taken from parameters, use that folder
          self.bgimage_path = self.parameter_images + "/"
      self.bgimage_index = 0
      self.bgimage_task = qi.PeriodicTask()
      self.bgimage_task.setCallback(self._next_image)
      self.bgimage_task.setUsPeriod(10000000)
      self.bgimage_task.start(True)
      return
    else:
      data["type"] = slide.get("tablet_type")
    data["text"] = self.replace_param(slide.get("text", None))
    data["fontSize"] = self.replace_param(slide.get("font_size"))
    tabletData = json.dumps(data)
    logger.info(tabletData[:100])
    self.module.showOnTablet(tabletData)

  def _next_image(self):
    if self.bgimage_stop or self.is_stopping:
      return
    image = self.bgimage_images[self.bgimage_index]
    slide = {
      "type": "tablet",
      "tablet_type": "image",
      "src": image
    }
    self.bgimage_index = (self.bgimage_index+1)%len(self.bgimage_images)
    self._show_image(slide, path=self.bgimage_path + image)

  def _animSay(self, text):
    if text!='':
      text = self.replace_param(text)
      text = random.choice(text.split(";"))
      text = "\\RSPD=75\\" + text
      self.module.session.service("ALAnimatedSpeech").say(text)

  def _play_sound(self, sound_file, isLoop=False):
    src_params = self._get_params_from_text(sound_file)
    if len(src_params) != 0:
      sound_file = self.parameters.get(src_params[0], sound_file)
    if self.parameters and self.parameters.get("type") == "custom":
      path = self.sounds_folder + "/" + sound_file
    else:
      path = self.pathToPackage + self.sounds_folder + "/" + sound_file
    logger.info(path)
    try:
      if isLoop:
        self.module.session.service(
          "ALAudioPlayer").playFileInLoop(path)
      else:
        self.module.session.service("ALAudioPlayer").playFile(path)
    except RuntimeError:
      logger.info("could not find sound, skipping")

  def _anim_sound(self, slide):
    sound_file = slide.get("sound")
    if self.current_app in ["similarityGame","quiz"]:
      if slide.get("sound_type") == u"custom":
        sound_file= self._get_sound_src(sound_file)
    if self.module.current_template == "sound therapy" and self.module.language in ["Malay", "Tamil"]:
      params = self._get_params_from_text(sound_file)
      logger.info("check those params: " + str(params))
      if len(params) == 1:
        sound_file = self.parameters.get(params[0])
      logger.info("resulting sound_file: " + str(sound_file))
    if isinstance(sound_file,list):
      sound_file= random.choice(sound_file)
    sound_file = self.replace_param(sound_file)
    if sound_file == "":
      return
    text = slide.get("text", "") + " \\pau=10000\\"
    sound_type = slide.get("sound_type")
    if sound_type == u"default":
      path = self.pathToPackage + self.speech_folder + "/" + sound_file
    elif sound_type == u"custom":
      if self.module.current_template == "reality orientation":
        path = self.parameter_images + "/../" + sound_file
      else:
        path = self.parameter_images + "/" + sound_file
    aTts = self.module.session.service("ALAnimatedSpeech")
    tts = self.module.session.service("ALTextToSpeech")
    player = self.module.session.service("ALAudioPlayer")
    try:
      if sound_file:
        aTts.say(text, _async=True)
        player.playFile(path)
        tts.stopAll()
      # self._change_volume(self.current_volume)
    except RuntimeError:
      logger.info("could not find sound, skipping\n" + path)

  def _background_music(self):
    """starts the background music thread"""
    self.music_must_stop = False
    qi.async(self._background_music_thread)

  def _background_music_thread(self):
    """keeps playing from the list music until music_must_stop"""
    i = 0
    music_list = self.parameters.get("music_list")
    sound_level = self.module.session.service(
      "ALAudioDevice").getOutputVolume()/100
    sound_level = max(sound_level-0.2, 0.1)
    player = self.module.session.service("ALAudioPlayer")
    while not self.music_must_stop:
      music_file = music_list[i]
      i = (i+1) % len(music_list)
      path = self.sounds_folder + music_file
      try:
        player.playFile(path)
      except RuntimeError:
        logger.info("backgroundMusic error: " + path)

  def _stop_music(self):
    self.music_must_stop = True
    self.module.session.service("ALAudioPlayer").stopAll()

  def _sound_loop(self, slide):
    bm = self.module.session.service("ALBehaviorManager")
    anim = slide["animation"]
    anim = self.replace_param(anim)
    if anim == "Stand/BodyTalk/Thinking/ThinkingLoop_1":
      anim = "Stand/BodyTalk/Thinking/ThinkingLoop_" + \
        random.choice(["1", "2"])
    elif anim == "normal":
      anim = "cgh-main/animations/soundTherapy/normal_loop"
    elif anim == "dancing":
      anim = "cgh-main/animations/soundTherapy/dancing_loop"
    bm.startBehavior(anim)
    duration = int(self.parameters.get("sound_duration", 30))
    # convert to micro seconds
    duration *= 1000000
    qi.async(self._stop_music, delay=duration)
    self._play_sound(slide["sound"], isLoop=True)
    bm.stopBehavior(anim)

  def _exercise_loop(self, slide):
    loops = slide["loops"]
    loops_params = self._get_params_from_text(loops)
    if len(loops_params) != 0:
      loops = self.parameters.get(loops_params[0], loops)
    self.module.exercise_n_loop = int(loops)
    anim = slide["animation"]
    anim_params = self._get_params_from_text(anim)
    if len(anim_params) != 0:
      anim = self.parameters.get(anim_params[0], anim)
    self._run_animation(anim)

  def _game_slideshow(self, slide):
    """shows a list of images/colors on the tablet, enumerates their name if available"""
    logger.info("here")
    logger.info(self.parameters)
    pause = slide.get("pause", 5)
    is_enum = slide.get("enumerate", 0)
    for item in self.parameters:
      if self.is_stopping:
        return
      image_slide = {
        "type": "tablet",
        "tablet_type": "image",
        "src": item["image"]
      }
      path = self.parameter_images + item["cat_id"] + "/" + item["image"]
      self._show_image(image_slide, path=path)
      if is_enum:
        if self.module.language in ["English","Chinese"]:
          self._animSay(item["name"])
        else:
          path = self.parameter_images + item["cat_id"] + "/" + item["sound"]
          self.module.session.service("ALAudioPlayer").playFile(path)
      time.sleep(pause)

  def _run_animation(self, animation):
    self.current_animation = animation
    self.module.session.service("ALBehaviorManager").runBehavior(animation)
    self.current_animation = None

  def _init_loop(self, slide):
    self.in_loop = True
    self.loop_slides = slide["slides"]
    self.loop_transition = slide.get("transition", None)
    self.is_sound_therapy = slide["loopParams"] == "soundsList"
    self.is_quiz = slide["loopParams"] == "quizList"
    self.is_similaritie_game = slide["loopParams"] == "sequenceList"
    self.is_memory_game = slide["loopParams"] == "setsList"
    self.is_categories_loop = slide["loopParams"] == "catsList"
    self.is_locations_loop = slide["loopParams"] == "locItems"
    self.loop_params_list = self.parameters[slide["loopParams"]]
    self.current_parameters = {}
    self.next_loop()

  def next_loop(self):
    self.skip_transition = False
    logger.info("back_loop? " + str(self.back_loop))
    if len(self.loop_params_list) > 0 and not self.is_stopping:
      if not self.back_loop:
        self.current_parameters = self.loop_params_list.pop(0)
        # we lose the sound's id when updating with all other parameters
        # so I save it here just in case it's sound_therapy
        # but only when loading a new loop, otherwise issue with loop replay
        if self.is_sound_therapy:
          self.sound_id = self.current_parameters.get("id")
      self.back_loop = False
      if self.is_sound_therapy:
        # loading sound_id here for convenience
        sound_id = self.sound_id
        if self.current_parameters.get("type") == "default":
          folder_path = "./data/soundTherapy/sounds/" + sound_id
        else:
          folder_path = path = self.module.data_folder + "/soundTherapy/" + \
            self.module.language + "/sounds/" + sound_id
        logger.info(folder_path)
        self.sub_player = SlidePlayer(
          SlideList(self.loop_slides),
          self.module,
          parameters=self.current_parameters,
          parameter_images=folder_path,
          sounds_folder=folder_path)
      elif self.is_quiz:
        folder_path = path = self.module.data_folder + "/quiz/" + \
          self.module.language + "/questions/" + self.current_parameters.get("id")
        logger.info(folder_path)
        self.sub_player = SlidePlayer(
          SlideList(self.loop_slides),
          self.module,
          parameters=self.current_parameters,
          parameter_images=folder_path,
          current_app="quiz",
          sounds_folder=folder_path,
          speech_folder ="./data/quiz/" + self.module.language)
      elif self.is_memory_game:
        folder_path = self.module.data_folder + "/memoryGame/" + \
          self.module.language + "/categories/"
        self.sub_player = SlidePlayer(
          SlideList(self.loop_slides),
          self.module,
          parameters=self.current_parameters,
          speech_folder=self.speech_folder,
          parameter_images=folder_path)
      elif self.is_similaritie_game:
        folder_path = self.module.data_folder + "/similarities/" + \
          self.module.language + "/sequences/" + self.current_parameters.get("id")
        logger.info(folder_path)
        self.sub_player = SlidePlayer(
          SlideList(self.loop_slides),
          self.module,
          parameters=self.current_parameters,
          parameter_images=folder_path,current_app="similarityGame",
          speech_folder="./data/similarityGame/" + self.module.language)
      elif self.is_categories_loop:
        logger.info("categories_loop")
        folder_path = self.module.data_folder + "/categories/" + self.module.language + "/locations/"
        logger.info(self.current_parameters)
        self.sub_player = SlidePlayer(
          SlideList(self.loop_slides),
          self.module,
          parameters=self.current_parameters,
          parameter_images=folder_path,
          speech_folder="./data/categories/" + self.module.language,
          images_folder="./data/categories/images")
      elif self.is_locations_loop:
        logger.info("locations_loop")
        folder_path = self.module.data_folder + "/categories/" + self.module.language + "/locations/"
        self.current_parameters["location"] = self.parameters["location"]
        self.current_parameters["location_image"] = self.parameters["location_image"]
        is_right_location = self.current_parameters["location_id"] == self.parameters["location_id"]
        logger.info("is from current location? " + str(is_right_location))
        if is_right_location:
          self.loop_slides = self.slides.get_current_slide()["slides"]
        else:
          self.loop_slides = self.slides.get_current_slide()["slides2"]
        logger.info(self.current_parameters)
        self.sub_player = SlidePlayer(
          SlideList(self.loop_slides),
          self.module,
          parameters=self.current_parameters,
          parameter_images=folder_path,
          speech_folder="./data/categories/" + self.module.language,
          images_folder="./data/categories/images")
      else:
        self.sub_player = SlidePlayer(
          SlideList(self.loop_slides),
          self.module,
          speech_folder=self.speech_folder,
          parameters=self.current_parameters)
      self.sub_player.register_finished_cb(self.on_loop_finished)
      logger.info("starting sub_player")
      self.sub_player.start()
    else:
      qi.async(self.next_slide)

  def play_loop_transition(self):
    if self.is_stopping:
      self.on_finished()
    self.sub_player = SlidePlayer(
      SlideList(self.loop_transition),
      self.module,
      parameters=self.parameters,
      speech_folder= self.speech_folder)
    self.sub_player.register_finished_cb(self.on_transition_finished)
    self.sub_player.start()

  def on_loop_finished(self):
    if len(self.loop_params_list) > 0 and not self.is_stopping:
      if self.loop_transition is not None and not self.skip_transition:
        self.play_loop_transition()
      else:
        self.next_loop()
    else:
      qi.async(self.next_slide)

  def on_transition_finished(self):
    self.next_loop()

  def skip_loop(self):
    if self.in_loop:
      self.skip_transition = False
      self.sub_player.stop()

  def replay_loop(self):
    if self.in_loop:
      self.skip_transition = True
      self.back_loop = True
      self.sub_player.stop()

  def on_finished(self):
    if self.bgimage_task != None:
      self.bgimage_task.stop()
    if self.on_finished_cb is not None:
      self.on_finished_cb()

  def resume(self):
    qi.async(self.process_slide)

  def replace_param(self, text, change_type=None):
    if text is None:
      return text
    language = self.module.language
    text_params = self._get_params_from_text(text)
    if len(text_params) > 0:
      logger.info((", ").join(text_params))
    for param in text_params:
      if change_type=="list":
        return self.parameters.get(param)

      elif param.startswith("CGH_"):
        logger.info("replace param: " + param)
        text = self.param_handler.replace_param(param, text, language)
        logger.info("replaced text: " + text)
      else:
        param_text = "<" + param + ">"
        text = text.replace(param_text, self.parameters.get(param))
    return text

  def _get_params_from_text(self, text):
    pattern = "<\\w+>"
    return [match.group(0)[1:-1] for match in re.finditer(pattern, text)]

  def _play_song(self, sound_file):
    bm = self.module.session.service("ALBehaviorManager")
    anim = "cgh-main/animations/soundTherapy/normal_loop"
    bm.startBehavior(anim)
    duration = int(self.parameters.get("song_duration",60))
    # convert to micro seconds
    duration *= 1000000
    qi.async(self._stop_music, delay=duration)
    self._play_sound(sound_file, isLoop=True)
    bm.stopBehavior(anim)

  def _get_current_volume(self):
    ad=self.module.session.service("ALAudioDevice")
    volume =ad.getOutputVolume()
    # logger.info("current volume: " + volume)
    return volume
  def _change_volume(self,volume):
    ad=self.module.session.service("ALAudioDevice")
    ad.setOutputVolume(volume)
    logger.info("changed:"+str(ad.getOutputVolume()))
    # logger.info("changed volume:"+str())

  def _get_sound_src(self,src):
    name_list =self._get_params_from_text(src)
    return self.parameters.get(name_list[0]) 





