# -*- coding: utf-8 -*-

import datetime

class ParamHandler(object):

  def __init__(self):
    self.months = {
      "English": ["", "January", "February", "March", "April", "May", "June",
                   "July", "August", "September", "October", "November", "December"],
      "Chinese": ["", u"1月", u"2月", u"3月", u"4月", u"5月", u"6月",
                  u"7月", u"8月", u"9月", u"10月", u"11月", u"12月"],
      "Tamil": ["", u"ஜனவரி", u"பிப்ரவரி", u"மார்ச்", u"ஏப்ரல்", u"மே", u"ஜூன்", u"ஜூலை",
                u"ஆகஸ்ட்", u"செப்டம்பர்", u"அக்டோபர்", u"நவம்பர்", u"டிசம்பர்"],
      "Malay": ["", u"Januari", u"Februari", u"Mac", u"April", u"Mungkin", u"Jun",
                  u"Julai", u"Ogos", u"September", u"Oktober", u"November", u"Disember"],
    }
    self.today = datetime.datetime.now()

  def replace_param(self, param, text, language="English"):
    self.today = datetime.datetime.now()
    param_text = "<"+param+">"
    if param == "CGH_MONTH":
      month = self.months[language][self.today.month]
      text = text.replace(param_text, month)
    elif param == "CGH_YEAR":
      text = text.replace(param_text, str(self.today.year))
    elif param == "CGH_DATE":
      text = text.replace(param_text, str(self.today.day))
    elif param == "CGH_TIME":
      time_text = self.today.strftime("%I:%M %p")
      text = text.replace(param_text, time_text)
    elif param == "CGH_TIME_TALK":
      hour = self.today.hour % 12
      minutes = self.today.minute
      am_pm = "am"
      if hour == 0:
        hour = 12
      if self.today.hour >= 12:
        am_pm = "pm"
      if language == "Chinese":
        time_text = str(hour) + u"点 " + str(minutes) + u"分 " + am_pm.upper()
      else:
        time_text = str(hour) + " " + str(minutes) + " " + am_pm
      text = text.replace(param_text, time_text)
    return text