

class SlideList(object):

  def __init__(self, list_of_slides):
    self.slides = list_of_slides
    self.slide_index = -1

  def next_slide(self):
    self.slide_index += 1
    return self.get_current_slide()

  def get_current_slide(self):
    if self.slide_index >= len(self.slides):
      return None
    else:
      return self.slides[self.slide_index]

  def reset_index(self):
    self.slide_index = -1