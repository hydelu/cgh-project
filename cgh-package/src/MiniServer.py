#!/usr/bin/env python
# Simple HTTP Server With Upload.

import os
import posixpath
import BaseHTTPServer
import urllib
import cgi
import shutil
import mimetypes
import re
import qi
try:
  from cStringIO import StringIO
except ImportError:
  from StringIO import StringIO

class SimpleHTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
  # Simple HTTP request handler with POST commands.

  def end_headers (self):
    self.send_header('Access-Control-Allow-Origin', '*')
    BaseHTTPServer.BaseHTTPRequestHandler.end_headers(self)

  def do_GET(self):
    self.send_response(200)
    self.send_header('Content-type', 'application/zip')
    self.send_header('Content-Disposition', 'attachment; filename="backup.zip"')
    self.end_headers()
    print self.path
    # not sure about this part below
    # path = qi.path.userWritableDataPath("CGHManager", "backup.zip")
    if os.path.exists(self.path):
      with open(self.path, 'rb') as f:
        self.wfile.write(f.read())

  def do_POST(self):
    """Serve a POST request."""
    r, info = self.deal_post_data()
    print r, info, "by: ", self.client_address
    f = StringIO()

    if r:
      f.write(info)
    else:
      f.write("<strong>Failed:" + info + "</strong>")

    length = f.tell()
    f.seek(0)
    self.send_response(200)
    self.send_header("Content-type", "text/html")
    self.send_header("Content-Length", str(length))
    self.end_headers()
    if f:
      self.copyfile(f, self.wfile)
      f.close()

  def deal_post_data(self):
    print self.headers
    boundary = self.headers.plisttext.split("=")[1]
    print 'Boundary %s' %boundary
    remainbytes = int(self.headers['content-length'])
    print "Remain Bytes %s" %remainbytes
    line = self.rfile.readline()
    remainbytes -= len(line)
    if not boundary in line:
      return (False, "Content NOT begin with boundary")
    line = self.rfile.readline()
    remainbytes -= len(line)
    fn = re.findall(r'Content-Disposition.*name=".*"; filename="(.*)"', line)
    if not fn:
      return (False, "Can't find out file name...")
    fn = qi.path.userWritableDataPath("CGHManager", "upload.zip")
    # fn = os.path.join(path, fn[0])
    line = self.rfile.readline()
    remainbytes -= len(line)
    line = self.rfile.readline()
    remainbytes -= len(line)
    try:
      out = open(fn, 'wb')
    except IOError:
      return (False, "Can't create file to write, do you have permission to write?")

    preline = self.rfile.readline()
    remainbytes -= len(preline)
    while remainbytes > 0:
      line = self.rfile.readline()
      remainbytes -= len(line)
      if boundary in line:
        preline = preline[0:-1]
        if preline.endswith('\r'):
          preline = preline[0:-1]
        out.write(preline)
        out.close()
        return (True, fn)
      else:
        out.write(preline)
        preline = line
    return (False, "Unexpect Ends of data.")

  def copyfile(self, source, outputfile):
    """Copy all data between two file objects.

    The SOURCE argument is a file object open for reading
    (or anything with a read() method) and the DESTINATION
    argument is a file object open for writing (or
    anything with a write() method).

    The only reason for overriding this would be to change
    the block size or perhaps to replace newlines by CRLF
    -- note however that this the default server uses this
    to copy binary data as well.

    """
    shutil.copyfileobj(source, outputfile)



def test(HandlerClass = SimpleHTTPRequestHandler,
     ServerClass = BaseHTTPServer.HTTPServer):
  BaseHTTPServer.test(HandlerClass, ServerClass)

if __name__ == '__main__':
  test()