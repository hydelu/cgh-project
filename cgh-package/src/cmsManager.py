# -*- coding: utf-8 -*-

import qi
import os
import shutil
import logging
import base64
import json
import subprocess
import requests
from which import which
import localization

SERVICE_NAME = "CGHManager"
PACKAGE_ID = "cgh-main"

logging.basicConfig(
    filename=qi.path.userWritableDataPath("CGHLogs", "CGHManager.log"),
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(SERVICE_NAME)
logger.info("STARTING SCRIPT")

class CMGManager(object):
  """receives and sends files for the CMS"""

  @qi.nobind
  def __init__(self, session):
    self.session = session
    self.user_data_path = qi.path.userWritableDataPath("/", "")
    self.data_folder_path = qi.path.userWritableDataPath("CGHTemplate", "")
    self.temp_data_path = qi.path.userWritableDataPath("CGHManager", "/")

  @qi.bind(qi.Bool, paramsType=[qi.String])
  def upload_backup(self, backup_path):
    logger.info("received back file")
    try:
      if os.path.exists(backup_path):
        if os.path.exists(self.data_folder_path):
          shutil.rmtree(self.data_folder_path)
        os.system("unzip " + backup_path + " -d " + self.user_data_path)
        logger.info("unzipped logger file")
    except Exception:
      return False
    return True

  @qi.bind(qi.String)
  def download_backup(self):
    logger.info("download_backup")
    if os.path.exists(self.data_folder_path):
      temp_path = self.temp_data_path + "backup.zip"
      if os.path.exists(temp_path):
        os.remove(temp_path)
      cmd = "zip -r ./CGHManager/backup.zip ./CGHTemplate"
      p = subprocess.Popen(cmd, cwd=self.user_data_path, shell=True)
      p.wait()
      if os.path.exists(temp_path):
        return temp_path

  @qi.bind(qi.String, paramsType=[qi.String])
  def create_backup(self, path):
    """creates a temporary backup file to be downloaded by the CMS, returns its path"""
    logger.info("creating backup file for folder: " + path)
    full_path = os.path.join(self.data_folder_path, path)
    if os.path.exists(full_path):
      temp_path = self.temp_data_path + "backup.zip"
      if os.path.exists(temp_path):
        os.remove(temp_path)
      target_path = "./CGHTemplate/" + path
      cmd = "zip -r ./CGHManager/backup.zip " + target_path
      p = subprocess.Popen(cmd, cwd=self.user_data_path, shell=True)
      p.wait()
      if os.path.exists(temp_path):
        return temp_path

  @qi.bind(qi.Bool, paramsType=[qi.String, qi.String])
  def install_backup(self, backup_path, target):
    """gets the zip at path and install it at target"""
    target_path = os.path.join(self.data_folder_path, target)
    print target_path
    try:
      if os.path.exists(backup_path):
        if os.path.exists(target_path):
          shutil.rmtree(target_path)
        os.system("unzip " + backup_path + " -d " + self.user_data_path)
    except Exception:
      return False
    return True

  @qi.bind(qi.String, paramsType=[qi.String])
  def get_json_from_path(self, path):
    logger.info("get_json: " + path)
    full_path = self.data_folder_path + "/" + path
    if os.path.exists(full_path):
      with open(full_path, 'r') as f:
        return f.read()
    return ""

  @qi.bind(qi.String, paramsType=[qi.String])
  def get_img_from_path(self, path):
    full_path = self.data_folder_path + "/" + path
    if os.path.exists(full_path):
      with open(full_path, 'r') as f:
        return base64.b64encode(f.read())
    return ""

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String])
  def save_image_to_path(self, path, data):
    full_path = self.data_folder_path + "/" + path
    self.create_folder_if_needed(full_path)
    logger.info("save_image_to_path: " + full_path)
    with open(full_path, 'w') as f:
      f.write(base64.b64decode(data))

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String])
  def save_json_to_path(self, path, data):
    full_path = self.data_folder_path + "/" + path
    self.create_folder_if_needed(full_path)
    with open(full_path, 'w') as f:
      f.write(data)

  @qi.bind(qi.Void, paramsType=[qi.String])
  def delete_file_from_path(self, path):
    full_path = self.data_folder_path + "/" + path
    if (os.path.exists(full_path)):
      os.remove(full_path)

  @qi.bind(qi.Void, paramsType=[qi.String])
  def delete_folder_from_path(self, path):
    full_path = self.data_folder_path + "/" + path
    if (os.path.exists(full_path)):
      shutil.rmtree(full_path)

  @qi.bind(qi.String, paramsType=[qi.String, qi.String])
  def get_presets_list(self, app, language):
    """Returns stringify JSON with list of [{"id": "", "config_name": ""}]"""
    folder_path = self.data_folder_path + "/" + app
    if language != "":
      folder_path += "/" + language
    folder_path += "/presets"
    file_names = next(os.walk(folder_path), (None, None, []))[2]
    logger.info(file_names)
    presets = []
    for file_name in file_names:
      if not file_name.endswith(".json"):
        continue
      file_path = folder_path + "/" + file_name
      name = ""
      with open(file_path) as f:
        file_data = json.load(f)
        name = file_data.get("config_name", "")
      presets.append({
        "id": file_name,
        "config_name": name
      })
    return json.dumps(presets)

  @qi.bind(qi.String, paramsType=[qi.String, qi.String])
  def get_roleplay_list(self, scenario, language):
    """Returns stringify JSON with list of complete data files"""
    all_presets = []
    folder_path = self.data_folder_path + "/roleplay/" + scenario + "/" + language
    try:
      custom_sounds = next(os.walk(folder_path), (None,None,None))[1]
      for preset in custom_sounds:
        try:
          data = None
          with open(folder_path + "/" + preset + "/data.json") as f:
            data  = json.load(f)
          all_presets.append(data)
        except Exception:
          continue
    except Exception as err:
      logger.warn(str(err))
    return json.dumps(all_presets)

  @qi.bind(qi.List(qi.String), paramsType=[qi.String])
  def get_files_list(self, folder):
    """Returns list of file names from folder"""
    folder_path = self.data_folder_path + "/" + folder
    logger.info(folder_path)
    files_list = next(os.walk(folder_path), (None, None, []))[2]
    logger.info(files_list)
    return files_list

  @qi.bind(qi.String, paramsType=[qi.String])
  def get_sounds_list(self, language):
    """Returns list of default and custom sounds available"""
    custom_folder_path = self.data_folder_path + "/soundTherapy/" + language + "/sounds"
    return self._load_data_files_from_folder(custom_folder_path)

  @qi.bind(qi.String, paramsType=[qi.String])
  def get_memory_categories_list(self, language):
    custom_folder_path = self.data_folder_path + "/memoryGame/" + language + "/categories"
    return self._load_data_files_from_folder(custom_folder_path)

  @qi.bind(qi.String, paramsType=[qi.String])
  def get_categories_locations_list(self, language):
    logger.info("get_categories_locations_list")
    custom_folder_path = self.data_folder_path + "/categories/" + language + "/locations"
    return self._load_data_files_from_folder(custom_folder_path)

  @qi.nobind
  def _load_data_files_from_folder(self, folder_path):
    all_data = []
    try:
      custom_sounds = next(os.walk(folder_path), (None,None,None))[1]
      for sound in custom_sounds:
        try:
          data = None
          with open(folder_path + "/" + sound + "/data.json") as f:
            data  = json.load(f)
          all_data.append(data)
        except Exception as err:
          continue
    except Exception as err:
      logger.warn(str(err))
    return json.dumps(all_data)

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.String])
  def save_reality_orientation_to_path(self, path, data, language):
    logger.info("save_reality_orientation_to_path")
    self.save_json_to_path(path, data)
    if language in ["Malay", "Tamil"]:
      qi.async(self.generate_ro_sound_files, data, language)

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.String])
  def save_memory_game_to_path(self, path, data, language):
    logger.info("save_memory_game_to_path")
    self.save_json_to_path(path + "data.json", data)
    j_data = json.loads(data)
    for item in j_data["items"]:
      text = item["name"]
      sound_name = item["sound"]
      res = self.download_tts(language, text)
      file_path = self.data_folder_path + "/" + path + sound_name[:-4] + ".mp3"
      open(file_path, "wb").write(res)
      self.amplify(file_path)
    logger.info("end of save_memory_game_to_path")

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.String])
  def save_category_game_to_path(self, path, data, language):
    logger.info("save_category_game_to_path")
    self.save_json_to_path(path + "data.json", data)
    j_data = json.loads(data)
    location_text = j_data.get("location_text")
    sentences = localization.category_game_sentences
    base_path = self.data_folder_path + "/" + path
    for pair in sentences[language]:
      text = pair[1].format(location_text)
      res = self.download_tts(language, text)
      file_path = base_path + pair[0] + ".mp3"
      open(file_path, "wb").write(res)
      self.amplify(file_path)
    logger.info("end of save_category_game_to_path")

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.String])
  def save_st_music_to_path(self, path, data, language):
    logger.info("save_st_music_to_path")
    j_data = json.loads(data)
    base_path = self.data_folder_path + "/" + path
    for comms in ["comment_0", "comment_1", "comment_2", "comment_3"]:
      sentences = j_data.get(comms).split(";")
      sounds_list = []
      for index, sentence in enumerate(sentences):
        file_path = base_path + comms + "_" + str(index) + ".mp3"
        res = self.download_tts(language, sentence)
        open(file_path, "wb").write(res)
        self.amplify(file_path)
        sounds_list.append(comms + "_" + str(index) + ".ogg")
      j_data[comms + "_sounds"] = sounds_list
    self.save_json_to_path(path + "data.json",json.dumps(j_data))
    logger.info("end of save_st_music_to_path")

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.String, qi.String])
  def save_roleplay_to_path(self, path, data, language, template):
    logger.info("save_roleplay_to_path")
    self.save_json_to_path(path + "data.json", data)
    j_data = json.loads(data)
    if template == "scenario1":
      sentences = localization.rp1_sentences
      base_path = self.data_folder_path + "/" + path
      for pair in sentences[language]:
        change_amount= j_data.get("wallet_amount") - j_data.get("second_item_cost")
        text = pair[1].format(
          main_food_text=j_data.get("main_food_text"),
          main_ingredient_text=j_data.get("main_ingredient_text"),
          correct_section_text=j_data.get("correct_section_text"),
          wrong_section_text=j_data.get("wrong_section_text"),
          wallet_amount=j_data.get("wallet_amount"),
          first_item=j_data.get("first_item"),
          first_item_cost=j_data.get("first_item_cost"),
          second_item=j_data.get("second_item"),
          second_item_cost=j_data.get("second_item_cost"),
          change_amount=change_amount,
          food_type=j_data.get("food_type"),
          correct_ingredient_text=j_data.get("correct_ingredient_text"),
          wrong_ingredient_text=j_data.get("wrong_ingredient_text")
        )
        res = self.download_tts(language, text)
        file_path = base_path + pair[0] + ".mp3"
        open(file_path, "wb").write(res)
        self.amplify(file_path)
    elif template == "scenario2":
      sentences = localization.rp2_sentences
      base_path = self.data_folder_path + "/" + path
      for pair in sentences[language]:
        cost_two_way = str(int(j_data.get("cost_one_way"))*2)
        text = pair[1].format(
          festival_name=j_data.get("festival_name"),
          gift_option_1=j_data.get("gift_option_1"),
          gift_option_2=j_data.get("gift_option_2"),
          gift_category=j_data.get("gift_category"),
          gift_fun_fact=j_data.get("gift_fun_fact"),
          mrt_destination=j_data.get("mrt_destination"),
          cost_one_way=j_data.get("cost_one_way"),
          ezlink_money=j_data.get("ezlink_money"),
          cost_two_way=cost_two_way,
          festival_greeting=j_data.get("festival_greeting"),
          festival_snack=j_data.get("festival_snack"),
          snack_description=j_data.get("snack_description")
        )
        res = self.download_tts(language, text)
        file_path = base_path + pair[0] + ".mp3"
        open(file_path, "wb").write(res)
        self.amplify(file_path)
    logger.info("end of save_roleplay_to_path")

  @qi.nobind
  def generate_ro_sound_files(self, data, language):
    # to be called async to generate the necessary files in the background
    data = json.loads(data)
    try:
      year = data[language]["months_comments"][0]
      prime = data[language]["prime_minister_name"]
      path = self.data_folder_path + "/realityOrientation/"
      sentences = {
        "ro_tamil_year": u"அது சரி; நாம் " + year + u" இருக்கிறோம் .",
        "ro_tamil_minister": u"அது சரி! அவர் " + prime + u" !",
        "ro_malay_year": "Ya, betul! Tahun ini tahun " + year + " !",
        "ro_malay_minister": "Ya betul, Perdana Menteri waktu ini ialah " + prime + " !"
      }
      file = "ro_" + language.lower() + "_year"
      res = self.download_tts(language, sentences[file])
      file_path = path + file + ".mp3"
      open(file_path, "wb").write(res)
      self.amplify(file_path)
      file = "ro_" + language.lower() + "_minister"
      res = self.download_tts(language, sentences[file])
      file_path = path + file + ".mp3"
      open(file_path, "wb").write(res)
      self.amplify(file_path)
      for month in range(1,13):
        logger.info("here " + str(month))
        file = "ro_" + language.lower() + "_month_desc_" + str(month) + "_"
        phrases = data[language]["months_comments"][month].split(";")
        for i, phrase in enumerate(phrases):
          res = self.download_tts(language, phrase)
          file_path = path + file + str(i) + ".mp3"
          open(file_path, "wb").write(res)
          self.amplify(file_path)
    except Exception as err:
      logger.info("error: " +  str(err))

  @qi.nobind
  def download_tts(self, language, sentence):
    # TODO will need to change url to SB account cloud function
    url = "http://asia-southeast1-softbanktelecom.cloudfunctions.net/texttospeech"
    # converts text to ssml, adds default voice speed parameter
    msg = '<speak><prosody rate="75%">' + sentence + '</prosody></speak>'
    data= {"message": msg, "language": language.lower()}
    res = requests.post(url, data=data)
    return res.content

  @qi.nobind
  def amplify(self, path):
    # this where I use subprocess ffmpeg to change the amplitude of each file
    # first check if ffmpeg is installed (always ok on robot, but dev machines might vary)
    new_path = path[:-4] + ".ogg"
    logger.info(path)
    logger.info(new_path)
    if (which("ffmpeg") is not None):
      # create ogg amplified version
      subprocess.call(["ffmpeg", "-i", path, "-af", "volume=2.5", "-c:a", "libvorbis", new_path, "-y", "-loglevel", "error"])
      # rm the mp3 version
      subprocess.call(["rm", path])

#newly added 22/2/22: for songs list in theme song
  @qi.bind(qi.String, paramsType=[qi.String])
  def get_songs_list(self, language):
    """Returns list of default and custom sounds available"""
    all_songs = []
    custom_folder_path = self.data_folder_path + "/themeSong/" + language + "/songs"
    try:
      custom_songs = next(os.walk(custom_folder_path), (None,None,None))[1]
      for song in custom_songs:
        try:
          data = None
          with open(custom_folder_path + "/" + song + "/data.json") as f:
            data  = json.load(f)
          all_songs.append(data)
        except Exception:
          continue
    except Exception as err:
      logger.warn(str(err))
    return json.dumps(all_songs)

  @qi.nobind
  def create_folder_if_needed(self, file_path):
    """gets folder path, checks if exists, creates if not"""
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
      os.makedirs(directory)

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String])
  def upload_folder(self, target_path, backup_data):
    logger.info("received backup file")
    path = self.temp_data_path + "backup.zip"
    decoded_data = base64.b64decode(backup_data)
    with open(path, 'w') as f:
      f.write(decoded_data)
    full_path = os.path.join(self.data_folder_path, target_path)
    parent_path = os.path.abspath(os.path.join(full_path, os.pardir))
    if os.path.exists(path):
      if os.path.exists(full_path):
        shutil.rmtree(full_path)
      self.create_folder_if_needed(parent_path)
      cmd = "unzip " + path + " -d " + parent_path
      logger.info(cmd)
      os.system(cmd)
      logger.info("unzipped logger file")

  @qi.bind(qi.String, paramsType=[qi.String])
  def download_folder(self, target_path):
    logger.info("download_backup")
    full_path = self.data_folder_path + "/" + target_path
    folder_name = os.path.basename(full_path)
    parent_folder = os.path.abspath(os.path.join(full_path, os.pardir))
    if os.path.exists(full_path):
      temp_path = self.temp_data_path + "backup.zip"
      if os.path.exists(temp_path):
        os.remove(temp_path)
      cmd = "zip -r " + temp_path + " ./" + folder_name
      p = subprocess.Popen(cmd, cwd=parent_folder, shell=True)
      p.wait()
      if os.path.exists(temp_path):
        with open(temp_path, 'r') as f:
          return base64.b64encode(f.read())
    return ""
  
  @qi.bind(qi.String, paramsType=[qi.String])
  def get_questions_list(self, language):
    all_questions = []
    custom_folder_path = self.data_folder_path + "/quiz/"+language+"/questions"
    try:
      custom_questions = next(os.walk(custom_folder_path), (None, None, None))[1]
      for question in custom_questions:
        try:
          data=None 
          with open(custom_folder_path + "/" + question + "/data.json") as f:
            data = json.load(f)
          all_questions.append(data)
        except Exception:
          continue
    except Exception as err:
      logger.warn(str(err))
    return json.dumps(all_questions)
  
  @qi.bind(qi.String, paramsType=[qi.String])
  def get_sequence_list(self, language):
    all_sequences = []
    custom_folder_path = self.data_folder_path + "/similarities/"+language+"/sequences"
    try:
      custom_sequences = next(os.walk(custom_folder_path), (None, None, None))[1]
      for sequence in custom_sequences:
        try:
          data=None 
          with open(custom_folder_path + "/" + sequence + "/data.json") as f:
            data = json.load(f)
          all_sequences.append(data)
        except Exception:
          continue
    except Exception as err:
      logger.warn(str(err))
    return json.dumps(all_sequences)
  
  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.String])
  def save_similarity_game_sequence(self,path,data,language):
    logger.info("save_similarity_game_sequence")
    if language in ["Malay", "Tamil"]:
      data=json.loads(data)
      sequence_question_1 =data.get("sequence_question_1")
      sequence_answer_1 = data.get("sequence_answer_1")
      seq_question2 = data.get("seq_question2")
      seq_answer2 = data.get("seq_answer2")
      data["s_q_1_sound"]=self.generate_sounds(path,language,sequence_question_1,"sequ_1")
      data["s_a_1_sound"]=self.generate_sounds(path,language,sequence_answer_1,"sean_1")
      if seq_question2:
        data["s_q_2_sound"]=self.generate_sounds(path,language,sequence_question_1,"sequ_2")
        data["s_a_2_sound"]=self.generate_sounds(path,language,seq_answer2,"sean_2")
      else:
         data["s_q_2_sound"]= ""
         data["s_a_2_sound"]= ""
      self.save_json_to_path(path+"/data.json", json.dumps(data))
  
  @qi.nobind
  def generate_sounds(self,path,language,sentance,name):
    src_list=[]
    sentance_list= sentance.split(";")
    for i in range(len(sentance_list)):
      res= self.download_tts(language,sentance_list[i])
      file_path =self.data_folder_path + "/" + path + name+"_"+str(i)+ ".mp3"
      open(file_path, "wb").write(res)
      self.amplify(file_path)
      new_path = name+"_"+str(i)+ ".ogg"
      src_list.append(new_path)
    return src_list

  @qi.bind(qi.Void, paramsType=[qi.String, qi.String, qi.String])
  def save_quez_questions(self,path,data,language):
    logger.info("save_quez_questions")
    if language in ["Malay", "Tamil"]:
      data=json.loads(data)
      data["quiz_question_sound"]= self.generate_sounds(path,language,data.get("quiz_question"),"quiz_question")
      data["quiz_answer_sound"]= self.generate_sounds(path,language,data.get("quiz_answer"),"quiz_answer")
      data["quiz_reminisce_sound"]= self.generate_sounds(path,language,data.get("quiz_reminisce"),"quiz_reminisce")
      self.save_json_to_path(path+"/data.json", json.dumps(data))
    
def main():
  import sys
  app = qi.Application(sys.argv)
  app.start()
  if "--testing" in sys.argv:
    global TESTING
    TESTING = True
  manager = CMGManager(app.session)
  app.session.registerService(SERVICE_NAME, manager)
  app.run()

if __name__ == '__main__':
  main()