<?xml version="1.0" encoding="UTF-8" ?>
<Package name="cgh-package" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="main" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/applause" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/happy_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0001" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0002" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0003" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0004" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0005" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0006" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0007" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0008" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0009" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0010" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0011" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0012" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0013" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0014" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0015" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0016" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/new_exercises" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0017" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0018" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0020" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0021" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0022" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/soundTherapy/normal_loop" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/soundTherapy/dancing_loop" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A1_Cupping_ear" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A2_Open_door" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A3_Gesture_2_tablet" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A4_Look_around" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A5_Ezlink_Topup" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A6_Ezlink_Gantry" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A7_MRT_handle" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A8_KnockOnDoor" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/A9_GiveOranges" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0019" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0000" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0023" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0024" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0025" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0026" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0027" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0028" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0029" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/exercises/0030" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/S6_Hungry_Growling" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/S3_Walking_Sounds" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/S5_pouring" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/S4_Frying_egg_fried_rice" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/S7_Washing_hands" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/roleplay/S8_Eating" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="applause" src="sounds/applause.ogg" />
        <File name="templateController" src="src/templateController.py" />
        <File name="paramHandler" src="src/paramHandler.py" />
        <File name="__init__" src="src/__init__.py" />
        <File name="slideList" src="src/slideList.py" />
        <File name="slidePlayer" src="src/slidePlayer.py" />
        <File name="roleplay" src="data/roleplay/English/roleplay.json" />
        <File name="cashier" src="data/roleplay/images/cashier.jpg" />
        <File name="check_mark" src="data/roleplay/images/check_mark.jpg" />
        <File name="dairy_section" src="data/roleplay/images/dairy_section.jpg" />
        <File name="egg_fried_rice" src="data/roleplay/images/egg_fried_rice.jpg" />
        <File name="eggs" src="data/roleplay/images/eggs.jpg" />
        <File name="ntuc_fairprice" src="data/roleplay/images/ntuc_fairprice.jpg" />
        <File name="pepper_name" src="data/roleplay/images/pepper_name.jpg" />
        <File name="poultry_section" src="data/roleplay/images/poultry_section.jpg" />
        <File name="wok_stir_fry" src="data/roleplay/images/wok_stir_fry.jpg" />
        <File name="A18-Go-to-Cashier" src="data/roleplay/sounds/A18-Go-to-Cashier.ogg" />
        <File name="A23-Cashier-Tiller" src="data/roleplay/sounds/A23-Cashier-Tiller.ogg" />
        <File name="A6-Arrive-at-supermarket" src="data/roleplay/sounds/A6-Arrive-at-supermarket.ogg" />
        <File name="exercises" src="data/exercises/English/exercises.json" />
        <File name="reality_orientation" src="data/realityOrientation/English/reality_orientation.json" />
        <File name="cgh" src="data/realityOrientation/images/cgh.jpg" />
        <File name="sound_therapy" src="data/soundTherapy/English/sound_therapy.json" />
        <File name="walk" src="data/roleplay/sounds/walk.ogg" />
        <File name="cmsManager" src="src/cmsManager.py" />
        <File name="HDB" src="data/roleplay/images/HDB.jpg" />
        <File name="empty_wok" src="data/roleplay/images/empty_wok.jpg" />
        <File name="rice" src="data/roleplay/images/rice.jpg" />
        <File name="wok_with_rice" src="data/roleplay/images/wok_with_rice.jpg" />
        <File name="frying" src="data/roleplay/sounds/frying.mp3" />
        <File name="wash_hands" src="data/roleplay/images/wash_hands.jpg" />
        <File name="eating" src="data/roleplay/sounds/eating.ogg" />
        <File name="bootstrap.min" src="html/cms/css/bootstrap.min.css" />
        <File name="favicon" src="html/cms/favicon.ico" />
        <File name="index" src="html/cms/index.html" />
        <File name="bootstrap.bundle.min" src="html/cms/js/bootstrap.bundle.min.js" />
        <File name="qimessaging" src="html/cms/js/qimessaging.js" />
        <File name="style" src="html/cms/style.css" />
        <File name="gif" src="html/cms/js/gif.js" />
        <File name="gif.worker" src="html/cms/js/gif.worker.js" />
        <File name="exercises" src="data/exercises/Chinese/exercises.json" />
        <File name="reality_orientation" src="data/realityOrientation/Chinese/reality_orientation.json" />
        <File name="roleplay" src="data/roleplay/Chinese/roleplay.json" />
        <File name="A18-Go-to-Cashier.old" src="data/roleplay/sounds/A18-Go-to-Cashier.old.ogg" />
        <File name="A23-Cashier-Tiller.old" src="data/roleplay/sounds/A23-Cashier-Tiller.old.ogg" />
        <File name="A6-Arrive-at-supermarket.old" src="data/roleplay/sounds/A6-Arrive-at-supermarket.old.ogg" />
        <File name="sound_therapy" src="data/soundTherapy/Chinese/sound_therapy.json" />
        <File name="collage_1" src="data/roleplay/images/collage_1.jpg" />
        <File name="collage_2" src="data/roleplay/images/collage_2.jpg" />
        <File name="collage_3" src="data/roleplay/images/collage_3.jpg" />
        <File name="collage_4" src="data/roleplay/images/collage_4.jpg" />
        <File name="eggs_section" src="data/roleplay/images/eggs_section.jpg" />
        <File name="empty_plate" src="data/roleplay/images/empty_plate.jpg" />
        <File name="kitchen" src="data/roleplay/images/kitchen.jpg" />
        <File name="ntuc_1973" src="data/roleplay/images/ntuc_1973.jpg" />
        <File name="oil_pour" src="data/roleplay/images/oil_pour.jpg" />
        <File name="red_x" src="data/roleplay/images/red_x.jpg" />
        <File name="wok_at_tze_char_setting" src="data/roleplay/images/wok_at_tze_char_setting.jpg" />
        <File name="door_open" src="animations/roleplay/A2_Open_door/door_open.wav" />
        <File name="ES_Beep" src="animations/roleplay/A5_Ezlink_Topup/ES_Beep.mp3" />
        <File name="ES_Beep_2" src="animations/roleplay/A6_Ezlink_Gantry/ES_Beep_2.mp3" />
        <File name="219668__harrietniamh__door-knocks" src="animations/roleplay/A8_KnockOnDoor/door-knocks.mp3" />
        <File name="10_Mandarin_Orange" src="data/roleplay/images/10_Mandarin_Orange.jpg" />
        <File name="11_House_and_Friends" src="data/roleplay/images/11_House_and_Friends.jpg" />
        <File name="12_MRT_Station" src="data/roleplay/images/12_MRT_Station.jpg" />
        <File name="13_Old_MRT" src="data/roleplay/images/13_Old_MRT.jpg" />
        <File name="14.1_EZLINK_3d" src="data/roleplay/images/14.1_EZLINK_3d.jpg" />
        <File name="14.2_EZLINK_3vs4" src="data/roleplay/images/14.2_EZLINK_3vs4.jpg" />
        <File name="14_EZLINK_Card" src="data/roleplay/images/14_EZLINK_Card.jpg" />
        <File name="18_Inside_of_MRT" src="data/roleplay/images/18_Inside_of_MRT.jpg" />
        <File name="19_Table_of_CNY_snacks" src="data/roleplay/images/19_Table_of_CNY_snacks.jpg" />
        <File name="1_Doorbell" src="data/roleplay/images/1_Doorbell.jpg" />
        <File name="20_Pineapple_tarts" src="data/roleplay/images/20_Pineapple_tarts.jpg" />
        <File name="21_Home" src="data/roleplay/images/21_Home.jpg" />
        <File name="2_Door" src="data/roleplay/images/2_Door.jpg" />
        <File name="3_Chinese_New_Year" src="data/roleplay/images/3_Chinese_New_Year.jpg" />
        <File name="4_people_gathering_cny" src="data/roleplay/images/4_people_gathering_cny.jpg" />
        <File name="5_Present" src="data/roleplay/images/5_Present.jpg" />
        <File name="6_MRT" src="data/roleplay/images/6_MRT.jpg" />
        <File name="7_Shopping_Mall" src="data/roleplay/images/7_Shopping_Mall.jpg" />
        <File name="8_Shopping_Mall_Entrance" src="data/roleplay/images/8_Shopping_Mall_Entrance.jpg" />
        <File name="9_OrangevsWatermelon" src="data/roleplay/images/9_OrangevsWatermelon.jpg" />
        <File name="S2_MRT_Noise" src="data/roleplay/sounds/S2_MRT_Noise.ogg" />
        <File name="S3_Train_running_sound" src="data/roleplay/sounds/S3_Train_running_sound.ogg" />
        <File name="roleplay2" src="data/roleplay/English/roleplay2.json" />
        <File name="16_Gantry" src="data/roleplay/images/16_Gantry.jpg" />
        <File name="17_MRT_Platform" src="data/roleplay/images/17_MRT_Platform.jpg" />
        <File name="sound_door-knob-turning" src="animations/roleplay/A2_Open_door/sound_door-knob-turning.ogg" />
        <File name="15_Topup_machine" src="data/roleplay/images/15_Topup_machine.jpg" />
        <File name="all.min" src="html/cms/css/all.min.css" />
        <File name="Font Awesome 5 Pro-Light-300" src="html/cms/font/Font Awesome 5 Pro-Light-300.otf" />
        <File name="Font Awesome 5 Pro-Regular-400" src="html/cms/font/Font Awesome 5 Pro-Regular-400.otf" />
        <File name="Font Awesome 5 Pro-Solid-900" src="html/cms/font/Font Awesome 5 Pro-Solid-900.otf" />
        <File name="Montserrat-Black" src="html/cms/font/Montserrat-Black.otf" />
        <File name="Montserrat-Bold" src="html/cms/font/Montserrat-Bold.otf" />
        <File name="Montserrat-Bold" src="html/cms/font/Montserrat-Bold.ttf" />
        <File name="Montserrat-ExtraBold" src="html/cms/font/Montserrat-ExtraBold.otf" />
        <File name="Montserrat-Hairline" src="html/cms/font/Montserrat-Hairline.otf" />
        <File name="Montserrat-Light" src="html/cms/font/Montserrat-Light.otf" />
        <File name="Montserrat-Regular" src="html/cms/font/Montserrat-Regular.otf" />
        <File name="Montserrat-Regular" src="html/cms/font/Montserrat-Regular.ttf" />
        <File name="Montserrat-SemiBold" src="html/cms/font/Montserrat-SemiBold.otf" />
        <File name="Montserrat-UltraLight" src="html/cms/font/Montserrat-UltraLight.otf" />
        <File name="fa-brands-400" src="html/cms/webfonts/fa-brands-400.eot" />
        <File name="fa-brands-400" src="html/cms/webfonts/fa-brands-400.svg" />
        <File name="fa-brands-400" src="html/cms/webfonts/fa-brands-400.ttf" />
        <File name="fa-brands-400" src="html/cms/webfonts/fa-brands-400.woff" />
        <File name="fa-brands-400" src="html/cms/webfonts/fa-brands-400.woff2" />
        <File name="fa-duotone-900" src="html/cms/webfonts/fa-duotone-900.eot" />
        <File name="fa-duotone-900" src="html/cms/webfonts/fa-duotone-900.svg" />
        <File name="fa-duotone-900" src="html/cms/webfonts/fa-duotone-900.ttf" />
        <File name="fa-duotone-900" src="html/cms/webfonts/fa-duotone-900.woff" />
        <File name="fa-duotone-900" src="html/cms/webfonts/fa-duotone-900.woff2" />
        <File name="fa-light-300" src="html/cms/webfonts/fa-light-300.eot" />
        <File name="fa-light-300" src="html/cms/webfonts/fa-light-300.svg" />
        <File name="fa-light-300" src="html/cms/webfonts/fa-light-300.ttf" />
        <File name="fa-light-300" src="html/cms/webfonts/fa-light-300.woff" />
        <File name="fa-light-300" src="html/cms/webfonts/fa-light-300.woff2" />
        <File name="fa-regular-400" src="html/cms/webfonts/fa-regular-400.eot" />
        <File name="fa-regular-400" src="html/cms/webfonts/fa-regular-400.svg" />
        <File name="fa-regular-400" src="html/cms/webfonts/fa-regular-400.ttf" />
        <File name="fa-regular-400" src="html/cms/webfonts/fa-regular-400.woff" />
        <File name="fa-regular-400" src="html/cms/webfonts/fa-regular-400.woff2" />
        <File name="fa-solid-900" src="html/cms/webfonts/fa-solid-900.eot" />
        <File name="fa-solid-900" src="html/cms/webfonts/fa-solid-900.svg" />
        <File name="fa-solid-900" src="html/cms/webfonts/fa-solid-900.ttf" />
        <File name="fa-solid-900" src="html/cms/webfonts/fa-solid-900.woff" />
        <File name="fa-solid-900" src="html/cms/webfonts/fa-solid-900.woff2" />
        <File name="MiniServer" src="src/MiniServer.py" />
        <File name="Stomach_Growl_fadeout" src="animations/roleplay/S6_Hungry_Growling/Stomach_Growl_fadeout.mp3" />
        <File name="Walking_Sound_Edited" src="animations/roleplay/S3_Walking_Sounds/Walking_Sound_Edited.ogg" />
        <File name="Fry_adding_oil_short" src="animations/roleplay/S5_pouring/Fry_adding_oil_short.ogg" />
        <File name="Frying_full" src="animations/roleplay/S4_Frying_egg_fried_rice/Frying_full.ogg" />
        <File name="WashHand" src="animations/roleplay/S7_Washing_hands/WashHand.ogg" />
        <File name="Eating" src="animations/roleplay/S8_Eating/Eating.ogg" />
        <File name="S1_Door_Bell_Ringing" src="animations/roleplay/A1_Cupping_ear/S1_Door_Bell_Ringing.ogg" />
        <File name="S3_Train_sound" src="animations/roleplay/A7_MRT_handle/S3_Train_sound.ogg" />
        <File name="SmileyFace" src="data/exercises/images/SmileyFace.png" />
        <File name="check_mark" src="data/realityOrientation/images/check_mark.png" />
        <File name="SmileyFace" src="data/soundTherapy/images/SmileyFace.png" />
        <File name="themeSong1" src="data/themeSong/English/themeSong1.json" />
        <File name="themeSong2" src="data/themeSong/English/themeSong2.json" />
        <File name="check_mark" src="data/themeSong/images/check_mark.png" />
        <File name="chunk-vendors.93d3c81a" src="html/cms/js/chunk-vendors.93d3c81a.js" />
        <File name="chunk-vendors.93d3c81a.js" src="html/cms/js/chunk-vendors.93d3c81a.js.map" />
        <File name="quiz" src="data/quiz/English/quiz.json" />
        <File name="icon" src="icon.png" />
        <File name="themeSong1" src="data/themeSong/Chinese/themeSong1.json" />
        <File name="themeSong2" src="data/themeSong/Chinese/themeSong2.json" />
        <File name="quiz" src="data/quiz/Chinese/quiz.json" />
        <File name="SmileyFace" src="data/quiz/images/SmileyFace.png" />
        <File name="template" src="data/memoryGame/English/template.json" />
        <File name="SmileyFace" src="data/memoryGame/images/SmileyFace.png" />
        <File name="roleplay2" src="data/roleplay/Chinese/roleplay2.json" />
        <File name="template" src="data/memoryGame/Chinese/template.json" />
        <File name="similarities" src="data/similarityGame/Chinese/similarities.json" />
        <File name="similarities" src="data/similarityGame/English/similarities.json" />
        <File name="SmileyFace" src="data/similarityGame/images/SmileyFace.png" />
        <File name="template" src="data/categories/English/template.json" />
        <File name="check_mark" src="data/categories/images/check_mark.png" />
        <File name="cross_mark" src="data/categories/images/cross_mark.jpg" />
        <File name="question" src="data/similarityGame/images/question.jpeg" />
        <File name="reality_orientation" src="data/realityOrientation/Tamil/reality_orientation.json" />
        <File name="reality_orientation" src="data/realityOrientation/Malay/reality_orientation.json" />
        <File name="all" src="html/launcher/css/all.css" />
        <File name="bootstrap.min" src="html/launcher/css/bootstrap.min.css" />
        <File name="favicon" src="html/launcher/favicon.ico" />
        <File name="Font Awesome 5 Pro-Light-300" src="html/launcher/font/Font Awesome 5 Pro-Light-300.otf" />
        <File name="Font Awesome 5 Pro-Regular-400" src="html/launcher/font/Font Awesome 5 Pro-Regular-400.otf" />
        <File name="Font Awesome 5 Pro-Solid-900" src="html/launcher/font/Font Awesome 5 Pro-Solid-900.otf" />
        <File name="Montserrat-Black" src="html/launcher/font/Montserrat-Black.otf" />
        <File name="Montserrat-Black" src="html/launcher/font/Montserrat-Black.ttf" />
        <File name="Montserrat-BlackItalic" src="html/launcher/font/Montserrat-BlackItalic.ttf" />
        <File name="Montserrat-Bold" src="html/launcher/font/Montserrat-Bold.otf" />
        <File name="Montserrat-Bold" src="html/launcher/font/Montserrat-Bold.ttf" />
        <File name="Montserrat-BoldItalic" src="html/launcher/font/Montserrat-BoldItalic.ttf" />
        <File name="Montserrat-ExtraBold" src="html/launcher/font/Montserrat-ExtraBold.otf" />
        <File name="Montserrat-ExtraBold" src="html/launcher/font/Montserrat-ExtraBold.ttf" />
        <File name="Montserrat-ExtraBoldItalic" src="html/launcher/font/Montserrat-ExtraBoldItalic.ttf" />
        <File name="Montserrat-ExtraLight" src="html/launcher/font/Montserrat-ExtraLight.ttf" />
        <File name="Montserrat-ExtraLightItalic" src="html/launcher/font/Montserrat-ExtraLightItalic.ttf" />
        <File name="Montserrat-Hairline" src="html/launcher/font/Montserrat-Hairline.otf" />
        <File name="Montserrat-Italic" src="html/launcher/font/Montserrat-Italic.ttf" />
        <File name="Montserrat-Light" src="html/launcher/font/Montserrat-Light.otf" />
        <File name="Montserrat-Light" src="html/launcher/font/Montserrat-Light.ttf" />
        <File name="Montserrat-LightItalic" src="html/launcher/font/Montserrat-LightItalic.ttf" />
        <File name="Montserrat-Medium" src="html/launcher/font/Montserrat-Medium.ttf" />
        <File name="Montserrat-MediumItalic" src="html/launcher/font/Montserrat-MediumItalic.ttf" />
        <File name="Montserrat-Regular" src="html/launcher/font/Montserrat-Regular.otf" />
        <File name="Montserrat-Regular" src="html/launcher/font/Montserrat-Regular.ttf" />
        <File name="Montserrat-SemiBold" src="html/launcher/font/Montserrat-SemiBold.otf" />
        <File name="Montserrat-SemiBold" src="html/launcher/font/Montserrat-SemiBold.ttf" />
        <File name="Montserrat-SemiBoldItalic" src="html/launcher/font/Montserrat-SemiBoldItalic.ttf" />
        <File name="Montserrat-Thin" src="html/launcher/font/Montserrat-Thin.ttf" />
        <File name="Montserrat-ThinItalic" src="html/launcher/font/Montserrat-ThinItalic.ttf" />
        <File name="Montserrat-UltraLight" src="html/launcher/font/Montserrat-UltraLight.otf" />
        <File name="index" src="html/launcher/index.html" />
        <File name="bootstrap.bundle.min" src="html/launcher/js/bootstrap.bundle.min.js" />
        <File name="chunk-vendors.93d3c81a" src="html/launcher/js/chunk-vendors.93d3c81a.js" />
        <File name="chunk-vendors.93d3c81a.js" src="html/launcher/js/chunk-vendors.93d3c81a.js.map" />
        <File name="qimessaging" src="html/launcher/js/qimessaging.js" />
        <File name="style" src="html/launcher/style.css" />
        <File name="style.css" src="html/launcher/style.css.old" />
        <File name="fa-brands-400" src="html/launcher/webfonts/fa-brands-400.eot" />
        <File name="fa-brands-400" src="html/launcher/webfonts/fa-brands-400.svg" />
        <File name="fa-brands-400" src="html/launcher/webfonts/fa-brands-400.ttf" />
        <File name="fa-brands-400" src="html/launcher/webfonts/fa-brands-400.woff" />
        <File name="fa-brands-400" src="html/launcher/webfonts/fa-brands-400.woff2" />
        <File name="fa-duotone-900" src="html/launcher/webfonts/fa-duotone-900.eot" />
        <File name="fa-duotone-900" src="html/launcher/webfonts/fa-duotone-900.svg" />
        <File name="fa-duotone-900" src="html/launcher/webfonts/fa-duotone-900.ttf" />
        <File name="fa-duotone-900" src="html/launcher/webfonts/fa-duotone-900.woff" />
        <File name="fa-duotone-900" src="html/launcher/webfonts/fa-duotone-900.woff2" />
        <File name="fa-light-300" src="html/launcher/webfonts/fa-light-300.eot" />
        <File name="fa-light-300" src="html/launcher/webfonts/fa-light-300.svg" />
        <File name="fa-light-300" src="html/launcher/webfonts/fa-light-300.ttf" />
        <File name="fa-light-300" src="html/launcher/webfonts/fa-light-300.woff" />
        <File name="fa-light-300" src="html/launcher/webfonts/fa-light-300.woff2" />
        <File name="fa-regular-400" src="html/launcher/webfonts/fa-regular-400.eot" />
        <File name="fa-regular-400" src="html/launcher/webfonts/fa-regular-400.svg" />
        <File name="fa-regular-400" src="html/launcher/webfonts/fa-regular-400.ttf" />
        <File name="fa-regular-400" src="html/launcher/webfonts/fa-regular-400.woff" />
        <File name="fa-regular-400" src="html/launcher/webfonts/fa-regular-400.woff2" />
        <File name="fa-solid-900" src="html/launcher/webfonts/fa-solid-900.eot" />
        <File name="fa-solid-900" src="html/launcher/webfonts/fa-solid-900.svg" />
        <File name="fa-solid-900" src="html/launcher/webfonts/fa-solid-900.ttf" />
        <File name="fa-solid-900" src="html/launcher/webfonts/fa-solid-900.woff" />
        <File name="fa-solid-900" src="html/launcher/webfonts/fa-solid-900.woff2" />
        <File name="which" src="src/which.py" />
        <File name="which" src="src/which.pyc" />
        <File name="ro_tamil_1" src="data/realityOrientation/Tamil/ro_tamil_1.ogg" />
        <File name="ro_tamil_10" src="data/realityOrientation/Tamil/ro_tamil_10.ogg" />
        <File name="ro_tamil_11" src="data/realityOrientation/Tamil/ro_tamil_11.ogg" />
        <File name="ro_tamil_12" src="data/realityOrientation/Tamil/ro_tamil_12.ogg" />
        <File name="ro_tamil_13" src="data/realityOrientation/Tamil/ro_tamil_13.ogg" />
        <File name="ro_tamil_2" src="data/realityOrientation/Tamil/ro_tamil_2.ogg" />
        <File name="ro_tamil_3" src="data/realityOrientation/Tamil/ro_tamil_3.ogg" />
        <File name="ro_tamil_4" src="data/realityOrientation/Tamil/ro_tamil_4.ogg" />
        <File name="ro_tamil_5" src="data/realityOrientation/Tamil/ro_tamil_5.ogg" />
        <File name="ro_tamil_6" src="data/realityOrientation/Tamil/ro_tamil_6.ogg" />
        <File name="ro_tamil_7" src="data/realityOrientation/Tamil/ro_tamil_7.ogg" />
        <File name="ro_tamil_8" src="data/realityOrientation/Tamil/ro_tamil_8.ogg" />
        <File name="ro_tamil_9" src="data/realityOrientation/Tamil/ro_tamil_9.ogg" />
        <File name="ro_tamil_date" src="data/realityOrientation/Tamil/ro_tamil_date.ogg" />
        <File name="ro_tamil_month_5" src="data/realityOrientation/Tamil/ro_tamil_month_5.ogg" />
        <File name="ro_malay_1" src="data/realityOrientation/Malay/ro_malay_1.ogg" />
        <File name="ro_malay_10" src="data/realityOrientation/Malay/ro_malay_10.ogg" />
        <File name="ro_malay_11" src="data/realityOrientation/Malay/ro_malay_11.ogg" />
        <File name="ro_malay_12" src="data/realityOrientation/Malay/ro_malay_12.ogg" />
        <File name="ro_malay_13" src="data/realityOrientation/Malay/ro_malay_13.ogg" />
        <File name="ro_malay_2" src="data/realityOrientation/Malay/ro_malay_2.ogg" />
        <File name="ro_malay_3" src="data/realityOrientation/Malay/ro_malay_3.ogg" />
        <File name="ro_malay_4" src="data/realityOrientation/Malay/ro_malay_4.ogg" />
        <File name="ro_malay_5" src="data/realityOrientation/Malay/ro_malay_5.ogg" />
        <File name="ro_malay_6" src="data/realityOrientation/Malay/ro_malay_6.ogg" />
        <File name="ro_malay_7" src="data/realityOrientation/Malay/ro_malay_7.ogg" />
        <File name="ro_malay_8" src="data/realityOrientation/Malay/ro_malay_8.ogg" />
        <File name="ro_malay_9" src="data/realityOrientation/Malay/ro_malay_9.ogg" />
        <File name="ro_malay_date" src="data/realityOrientation/Malay/ro_malay_date.ogg" />
        <File name="ro_malay_month" src="data/realityOrientation/Malay/ro_malay_month.ogg" />
        <File name="" src=".DS_Store" />
        <File name="" src="animations/.DS_Store" />
        <File name="" src="animations/exercises/.DS_Store" />
        <File name="" src="animations/roleplay/.DS_Store" />
        <File name="" src="animations/roleplay/A1_Cupping_ear/.DS_Store" />
        <File name="" src="animations/roleplay/A2_Open_door/.DS_Store" />
        <File name="" src="animations/roleplay/A3_Gesture_2_tablet/.DS_Store" />
        <File name="" src="animations/roleplay/A4_Look_around/.DS_Store" />
        <File name="" src="animations/roleplay/A5_Ezlink_Topup/.DS_Store" />
        <File name="" src="animations/roleplay/A6_Ezlink_Gantry/.DS_Store" />
        <File name="" src="animations/roleplay/A7_MRT_handle/.DS_Store" />
        <File name="" src="animations/roleplay/A8_KnockOnDoor/.DS_Store" />
        <File name="" src="animations/roleplay/A9_GiveOranges/.DS_Store" />
        <File name="" src="animations/soundTherapy/.DS_Store" />
        <File name="" src="data/.DS_Store" />
        <File name="" src="data/exercises/.DS_Store" />
        <File name="" src="data/exercises/images/.DS_Store" />
        <File name="mem_malay_1" src="data/memoryGame/Malay/mem_malay_1.ogg" />
        <File name="mem_malay_10" src="data/memoryGame/Malay/mem_malay_10.ogg" />
        <File name="mem_malay_11_0" src="data/memoryGame/Malay/mem_malay_11_0.ogg" />
        <File name="mem_malay_11_1" src="data/memoryGame/Malay/mem_malay_11_1.ogg" />
        <File name="mem_malay_2" src="data/memoryGame/Malay/mem_malay_2.ogg" />
        <File name="mem_malay_3" src="data/memoryGame/Malay/mem_malay_3.ogg" />
        <File name="mem_malay_4_0" src="data/memoryGame/Malay/mem_malay_4_0.ogg" />
        <File name="mem_malay_4_1" src="data/memoryGame/Malay/mem_malay_4_1.ogg" />
        <File name="mem_malay_5_0" src="data/memoryGame/Malay/mem_malay_5_0.ogg" />
        <File name="mem_malay_5_1" src="data/memoryGame/Malay/mem_malay_5_1.ogg" />
        <File name="mem_malay_5_2" src="data/memoryGame/Malay/mem_malay_5_2.ogg" />
        <File name="mem_malay_6_0" src="data/memoryGame/Malay/mem_malay_6_0.ogg" />
        <File name="mem_malay_6_1" src="data/memoryGame/Malay/mem_malay_6_1.ogg" />
        <File name="mem_malay_6_2" src="data/memoryGame/Malay/mem_malay_6_2.ogg" />
        <File name="mem_malay_7_0" src="data/memoryGame/Malay/mem_malay_7_0.ogg" />
        <File name="mem_malay_7_1" src="data/memoryGame/Malay/mem_malay_7_1.ogg" />
        <File name="mem_malay_8_0" src="data/memoryGame/Malay/mem_malay_8_0.ogg" />
        <File name="mem_malay_8_1" src="data/memoryGame/Malay/mem_malay_8_1.ogg" />
        <File name="mem_malay_9_0" src="data/memoryGame/Malay/mem_malay_9_0.ogg" />
        <File name="mem_malay_9_1" src="data/memoryGame/Malay/mem_malay_9_1.ogg" />
        <File name="template" src="data/memoryGame/Malay/template.json" />
        <File name="QZ_Malay_1" src="data/quiz/Malay/QZ_Malay_1.ogg" />
        <File name="QZ_Malay_2" src="data/quiz/Malay/QZ_Malay_2.ogg" />
        <File name="QZ_Malay_3" src="data/quiz/Malay/QZ_Malay_3.ogg" />
        <File name="QZ_Malay_4" src="data/quiz/Malay/QZ_Malay_4.ogg" />
        <File name="QZ_Malay_5_1" src="data/quiz/Malay/QZ_Malay_5_1.ogg" />
        <File name="QZ_Malay_5_2" src="data/quiz/Malay/QZ_Malay_5_2.ogg" />
        <File name="QZ_Malay_6_1" src="data/quiz/Malay/QZ_Malay_6_1.ogg" />
        <File name="QZ_Malay_6_2" src="data/quiz/Malay/QZ_Malay_6_2.ogg" />
        <File name="QZ_Malay_7_1" src="data/quiz/Malay/QZ_Malay_7_1.ogg" />
        <File name="QZ_Malay_7_2" src="data/quiz/Malay/QZ_Malay_7_2.ogg" />
        <File name="QZ_Malay_7_3" src="data/quiz/Malay/QZ_Malay_7_3.ogg" />
        <File name="quiz" src="data/quiz/Malay/quiz.json" />
        <File name="QZ_Tamil_1" src="data/quiz/Tamil/QZ_Tamil_1.ogg" />
        <File name="QZ_Tamil_2" src="data/quiz/Tamil/QZ_Tamil_2.ogg" />
        <File name="QZ_Tamil_3" src="data/quiz/Tamil/QZ_Tamil_3.ogg" />
        <File name="QZ_Tamil_4" src="data/quiz/Tamil/QZ_Tamil_4.ogg" />
        <File name="QZ_Tamil_5_1" src="data/quiz/Tamil/QZ_Tamil_5_1.ogg" />
        <File name="QZ_Tamil_5_2" src="data/quiz/Tamil/QZ_Tamil_5_2.ogg" />
        <File name="QZ_Tamil_6_1" src="data/quiz/Tamil/QZ_Tamil_6_1.ogg" />
        <File name="QZ_Tamil_6_2" src="data/quiz/Tamil/QZ_Tamil_6_2.ogg" />
        <File name="QZ_Tamil_7_1" src="data/quiz/Tamil/QZ_Tamil_7_1.ogg" />
        <File name="QZ_Tamil_7_2" src="data/quiz/Tamil/QZ_Tamil_7_2.ogg" />
        <File name="QZ_Tamil_7_3" src="data/quiz/Tamil/QZ_Tamil_7_3.ogg" />
        <File name="quiz" src="data/quiz/Tamil/quiz.json" />
        <File name="" src="data/realityOrientation/.DS_Store" />
        <File name="" src="data/realityOrientation/English/.DS_Store" />
        <File name="" src="data/realityOrientation/images/.DS_Store" />
        <File name="" src="data/roleplay/.DS_Store" />
        <File name="similarities" src="data/similarityGame/Malay/similarities.json" />
        <File name="similarities_malay_1" src="data/similarityGame/Malay/similarities_malay_1.ogg" />
        <File name="similarities_malay_2" src="data/similarityGame/Malay/similarities_malay_2.ogg" />
        <File name="similarities_malay_3" src="data/similarityGame/Malay/similarities_malay_3.ogg" />
        <File name="similarities_malay_4_1" src="data/similarityGame/Malay/similarities_malay_4_1.ogg" />
        <File name="similarities_malay_4_2" src="data/similarityGame/Malay/similarities_malay_4_2.ogg" />
        <File name="similarities_malay_4_3" src="data/similarityGame/Malay/similarities_malay_4_3.ogg" />
        <File name="similarities_malay_4_4" src="data/similarityGame/Malay/similarities_malay_4_4.ogg" />
        <File name="similarities_malay_5_1" src="data/similarityGame/Malay/similarities_malay_5_1.ogg" />
        <File name="similarities_malay_5_2" src="data/similarityGame/Malay/similarities_malay_5_2.ogg" />
        <File name="similarities_malay_5_3" src="data/similarityGame/Malay/similarities_malay_5_3.ogg" />
        <File name="similarities_malay_5_4" src="data/similarityGame/Malay/similarities_malay_5_4.ogg" />
        <File name="similarities_malay_6_1" src="data/similarityGame/Malay/similarities_malay_6_1.ogg" />
        <File name="similarities_malay_6_2" src="data/similarityGame/Malay/similarities_malay_6_2.ogg" />
        <File name="similarities_malay_7_1" src="data/similarityGame/Malay/similarities_malay_7_1.ogg" />
        <File name="similarities_malay_7_2" src="data/similarityGame/Malay/similarities_malay_7_2.ogg" />
        <File name="similarities_malay_8_1" src="data/similarityGame/Malay/similarities_malay_8_1.ogg" />
        <File name="similarities_malay_8_2" src="data/similarityGame/Malay/similarities_malay_8_2.ogg" />
        <File name="similarities" src="data/similarityGame/Tamil/similarities.json" />
        <File name="similarities_Tamil_4_1" src="data/similarityGame/Tamil/similarities_Tamil_4_1.ogg" />
        <File name="similarities_Tamil_4_2" src="data/similarityGame/Tamil/similarities_Tamil_4_2.ogg" />
        <File name="similarities_Tamil_4_3" src="data/similarityGame/Tamil/similarities_Tamil_4_3.ogg" />
        <File name="similarities_Tamil_4_4" src="data/similarityGame/Tamil/similarities_Tamil_4_4.ogg" />
        <File name="similarities_Tamil_5_1" src="data/similarityGame/Tamil/similarities_Tamil_5_1.ogg" />
        <File name="similarities_Tamil_5_2" src="data/similarityGame/Tamil/similarities_Tamil_5_2.ogg" />
        <File name="similarities_Tamil_5_3" src="data/similarityGame/Tamil/similarities_Tamil_5_3.ogg" />
        <File name="similarities_Tamil_5_4" src="data/similarityGame/Tamil/similarities_Tamil_5_4.ogg" />
        <File name="similarities_Tamil_6_1" src="data/similarityGame/Tamil/similarities_Tamil_6_1.ogg" />
        <File name="similarities_Tamil_6_2" src="data/similarityGame/Tamil/similarities_Tamil_6_2.ogg" />
        <File name="similarities_Tamil_7_1" src="data/similarityGame/Tamil/similarities_Tamil_7_1.ogg" />
        <File name="similarities_Tamil_7_2" src="data/similarityGame/Tamil/similarities_Tamil_7_2.ogg" />
        <File name="similarities_Tamil_8_1" src="data/similarityGame/Tamil/similarities_Tamil_8_1.ogg" />
        <File name="similarities_Tamil_8_2" src="data/similarityGame/Tamil/similarities_Tamil_8_2.ogg" />
        <File name="similarities_tamil_1" src="data/similarityGame/Tamil/similarities_tamil_1.ogg" />
        <File name="similarities_tamil_2" src="data/similarityGame/Tamil/similarities_tamil_2.ogg" />
        <File name="similarities_tamil_3" src="data/similarityGame/Tamil/similarities_tamil_3.ogg" />
        <File name="" src="data/soundTherapy/.DS_Store" />
        <File name="" src="data/soundTherapy/images/.DS_Store" />
        <File name="" src="data/soundTherapy/sounds/.DS_Store" />
        <File name="TS1_malay_1" src="data/themeSong/Malay/TS1_malay_1.ogg" />
        <File name="TS1_malay_2_1" src="data/themeSong/Malay/TS1_malay_2_1.ogg" />
        <File name="TS1_malay_2_2" src="data/themeSong/Malay/TS1_malay_2_2.ogg" />
        <File name="TS1_malay_3" src="data/themeSong/Malay/TS1_malay_3.ogg" />
        <File name="TS1_malay_4" src="data/themeSong/Malay/TS1_malay_4.ogg" />
        <File name="TS2_malay_1" src="data/themeSong/Malay/TS2_malay_1.ogg" />
        <File name="TS2_malay_2_1" src="data/themeSong/Malay/TS2_malay_2_1.ogg" />
        <File name="TS2_malay_2_2" src="data/themeSong/Malay/TS2_malay_2_2.ogg" />
        <File name="TS2_malay_3" src="data/themeSong/Malay/TS2_malay_3.ogg" />
        <File name="TS_malay_step1_1" src="data/themeSong/Malay/TS_malay_step1_1.ogg" />
        <File name="TS_malay_step1_2" src="data/themeSong/Malay/TS_malay_step1_2.ogg" />
        <File name="TS_malay_step1_3" src="data/themeSong/Malay/TS_malay_step1_3.ogg" />
        <File name="TS_malay_step1_3_2" src="data/themeSong/Malay/TS_malay_step1_3_2.ogg" />
        <File name="TS_malay_step2_1" src="data/themeSong/Malay/TS_malay_step2_1.ogg" />
        <File name="TS_malay_step2_2" src="data/themeSong/Malay/TS_malay_step2_2.ogg" />
        <File name="TS_malay_step2_3_1" src="data/themeSong/Malay/TS_malay_step2_3_1.ogg" />
        <File name="TS_malay_step2_3_2" src="data/themeSong/Malay/TS_malay_step2_3_2.ogg" />
        <File name="intro1" src="data/themeSong/Malay/intro1.json" />
        <File name="intro2" src="data/themeSong/Malay/intro2.json" />
        <File name="themeSong1" src="data/themeSong/Malay/themeSong1.json" />
        <File name="themeSong2" src="data/themeSong/Malay/themeSong2.json" />
        <File name="TS1_Tamil_1" src="data/themeSong/Tamil/TS1_Tamil_1.ogg" />
        <File name="TS1_Tamil_2_1" src="data/themeSong/Tamil/TS1_Tamil_2_1.ogg" />
        <File name="TS1_Tamil_2_2" src="data/themeSong/Tamil/TS1_Tamil_2_2.ogg" />
        <File name="TS1_Tamil_3" src="data/themeSong/Tamil/TS1_Tamil_3.ogg" />
        <File name="TS1_Tamil_4" src="data/themeSong/Tamil/TS1_Tamil_4.ogg" />
        <File name="TS2_Tamil_1" src="data/themeSong/Tamil/TS2_Tamil_1.ogg" />
        <File name="TS2_Tamil_2_1" src="data/themeSong/Tamil/TS2_Tamil_2_1.ogg" />
        <File name="TS2_Tamil_2_2" src="data/themeSong/Tamil/TS2_Tamil_2_2.ogg" />
        <File name="TS2_Tamil_3" src="data/themeSong/Tamil/TS2_Tamil_3.ogg" />
        <File name="TS_Tamil_step1_1" src="data/themeSong/Tamil/TS_Tamil_step1_1.ogg" />
        <File name="TS_Tamil_step1_2" src="data/themeSong/Tamil/TS_Tamil_step1_2.ogg" />
        <File name="TS_Tamil_step1_3_1" src="data/themeSong/Tamil/TS_Tamil_step1_3_1.ogg" />
        <File name="TS_Tamil_step1_3_2" src="data/themeSong/Tamil/TS_Tamil_step1_3_2.ogg" />
        <File name="TS_Tamil_step2_1" src="data/themeSong/Tamil/TS_Tamil_step2_1.ogg" />
        <File name="TS_Tamil_step2_2" src="data/themeSong/Tamil/TS_Tamil_step2_2.ogg" />
        <File name="TS_Tamil_step2_3_1" src="data/themeSong/Tamil/TS_Tamil_step2_3_1.ogg" />
        <File name="TS_Tamil_step2_3_2" src="data/themeSong/Tamil/TS_Tamil_step2_3_2.ogg" />
        <File name="intro1" src="data/themeSong/Tamil/intro1.json" />
        <File name="intro2" src="data/themeSong/Tamil/intro2.json" />
        <File name="themeSong1" src="data/themeSong/Tamil/themeSong1.json" />
        <File name="themeSong2" src="data/themeSong/Tamil/themeSong2.json" />
        <File name="" src="html/.DS_Store" />
        <File name="app.0f40bacc" src="html/launcher/css/app.0f40bacc.css" />
        <File name="app.3ca8702d" src="html/launcher/js/app.3ca8702d.js" />
        <File name="app.3ca8702d.js" src="html/launcher/js/app.3ca8702d.js.map" />
        <File name="" src="sounds/.DS_Store" />
        <File name="" src="src/.DS_Store" />
        <File name="__init__" src="src/__init__.pyc" />
        <File name="paramHandler" src="src/paramHandler.pyc" />
        <File name="slideList" src="src/slideList.pyc" />
        <File name="slidePlayer" src="src/slidePlayer.pyc" />
        <File name="templateController" src="src/templateController.pyc" />
        <File name="mem_tamil_1" src="data/memoryGame/Tamil/mem_tamil_1.ogg" />
        <File name="mem_tamil_10" src="data/memoryGame/Tamil/mem_tamil_10.ogg" />
        <File name="mem_tamil_11" src="data/memoryGame/Tamil/mem_tamil_11.ogg" />
        <File name="mem_tamil_2" src="data/memoryGame/Tamil/mem_tamil_2.ogg" />
        <File name="mem_tamil_3" src="data/memoryGame/Tamil/mem_tamil_3.ogg" />
        <File name="mem_tamil_4_0" src="data/memoryGame/Tamil/mem_tamil_4_0.ogg" />
        <File name="mem_tamil_4_1" src="data/memoryGame/Tamil/mem_tamil_4_1.ogg" />
        <File name="mem_tamil_5_0" src="data/memoryGame/Tamil/mem_tamil_5_0.ogg" />
        <File name="mem_tamil_5_1" src="data/memoryGame/Tamil/mem_tamil_5_1.ogg" />
        <File name="mem_tamil_5_2" src="data/memoryGame/Tamil/mem_tamil_5_2.ogg" />
        <File name="mem_tamil_6_0" src="data/memoryGame/Tamil/mem_tamil_6_0.ogg" />
        <File name="mem_tamil_6_1" src="data/memoryGame/Tamil/mem_tamil_6_1.ogg" />
        <File name="mem_tamil_6_2" src="data/memoryGame/Tamil/mem_tamil_6_2.ogg" />
        <File name="mem_tamil_7_0" src="data/memoryGame/Tamil/mem_tamil_7_0.ogg" />
        <File name="mem_tamil_7_1" src="data/memoryGame/Tamil/mem_tamil_7_1.ogg" />
        <File name="mem_tamil_8_0" src="data/memoryGame/Tamil/mem_tamil_8_0.ogg" />
        <File name="mem_tamil_8_1" src="data/memoryGame/Tamil/mem_tamil_8_1.ogg" />
        <File name="mem_tamil_9_0" src="data/memoryGame/Tamil/mem_tamil_9_0.ogg" />
        <File name="mem_tamil_9_1" src="data/memoryGame/Tamil/mem_tamil_9_1.ogg" />
        <File name="template" src="data/memoryGame/Tamil/template.json" />
        <File name="cat_malay_1" src="data/categories/Malay/cat_malay_1.ogg" />
        <File name="cat_malay_10" src="data/categories/Malay/cat_malay_10.ogg" />
        <File name="cat_malay_11" src="data/categories/Malay/cat_malay_11.ogg" />
        <File name="cat_malay_2" src="data/categories/Malay/cat_malay_2.ogg" />
        <File name="cat_malay_3" src="data/categories/Malay/cat_malay_3.ogg" />
        <File name="cat_malay_4_0" src="data/categories/Malay/cat_malay_4_0.ogg" />
        <File name="cat_malay_4_1" src="data/categories/Malay/cat_malay_4_1.ogg" />
        <File name="cat_malay_4_2" src="data/categories/Malay/cat_malay_4_2.ogg" />
        <File name="cat_malay_5_0" src="data/categories/Malay/cat_malay_5_0.ogg" />
        <File name="cat_malay_5_1" src="data/categories/Malay/cat_malay_5_1.ogg" />
        <File name="cat_malay_5_2" src="data/categories/Malay/cat_malay_5_2.ogg" />
        <File name="cat_malay_6_0" src="data/categories/Malay/cat_malay_6_0.ogg" />
        <File name="cat_malay_6_1" src="data/categories/Malay/cat_malay_6_1.ogg" />
        <File name="cat_malay_6_2" src="data/categories/Malay/cat_malay_6_2.ogg" />
        <File name="cat_malay_7_0" src="data/categories/Malay/cat_malay_7_0.ogg" />
        <File name="cat_malay_7_1" src="data/categories/Malay/cat_malay_7_1.ogg" />
        <File name="cat_malay_8_0" src="data/categories/Malay/cat_malay_8_0.ogg" />
        <File name="cat_malay_8_1" src="data/categories/Malay/cat_malay_8_1.ogg" />
        <File name="cat_malay_8_2" src="data/categories/Malay/cat_malay_8_2.ogg" />
        <File name="cat_malay_9_0" src="data/categories/Malay/cat_malay_9_0.ogg" />
        <File name="cat_malay_9_1" src="data/categories/Malay/cat_malay_9_1.ogg" />
        <File name="template" src="data/categories/Malay/template.json" />
        <File name="template" src="data/categories/Tamil/template.json" />
        <File name="cat_tamil_1" src="data/categories/Tamil/cat_tamil_1.ogg" />
        <File name="cat_tamil_10" src="data/categories/Tamil/cat_tamil_10.ogg" />
        <File name="cat_tamil_11" src="data/categories/Tamil/cat_tamil_11.ogg" />
        <File name="cat_tamil_2" src="data/categories/Tamil/cat_tamil_2.ogg" />
        <File name="cat_tamil_3" src="data/categories/Tamil/cat_tamil_3.ogg" />
        <File name="cat_tamil_4_0" src="data/categories/Tamil/cat_tamil_4_0.ogg" />
        <File name="cat_tamil_4_1" src="data/categories/Tamil/cat_tamil_4_1.ogg" />
        <File name="cat_tamil_4_2" src="data/categories/Tamil/cat_tamil_4_2.ogg" />
        <File name="cat_tamil_5_0" src="data/categories/Tamil/cat_tamil_5_0.ogg" />
        <File name="cat_tamil_5_1" src="data/categories/Tamil/cat_tamil_5_1.ogg" />
        <File name="cat_tamil_5_2" src="data/categories/Tamil/cat_tamil_5_2.ogg" />
        <File name="cat_tamil_6_0" src="data/categories/Tamil/cat_tamil_6_0.ogg" />
        <File name="cat_tamil_6_1" src="data/categories/Tamil/cat_tamil_6_1.ogg" />
        <File name="cat_tamil_6_2" src="data/categories/Tamil/cat_tamil_6_2.ogg" />
        <File name="cat_tamil_7_0" src="data/categories/Tamil/cat_tamil_7_0.ogg" />
        <File name="cat_tamil_7_1" src="data/categories/Tamil/cat_tamil_7_1.ogg" />
        <File name="cat_tamil_8_0" src="data/categories/Tamil/cat_tamil_8_0.ogg" />
        <File name="cat_tamil_8_1" src="data/categories/Tamil/cat_tamil_8_1.ogg" />
        <File name="cat_tamil_8_2" src="data/categories/Tamil/cat_tamil_8_2.ogg" />
        <File name="cat_tamil_9_0" src="data/categories/Tamil/cat_tamil_9_0.ogg" />
        <File name="cat_tamil_9_1" src="data/categories/Tamil/cat_tamil_9_1.ogg" />
        <File name="" src="data/roleplay/Malay/.DS_Store" />
        <File name="roleplay" src="data/roleplay/Malay/roleplay.json" />
        <File name="rp1_malay_1" src="data/roleplay/Malay/rp1_malay_1.ogg" />
        <File name="rp1_malay_10" src="data/roleplay/Malay/rp1_malay_10.ogg" />
        <File name="rp1_malay_11" src="data/roleplay/Malay/rp1_malay_11.ogg" />
        <File name="rp1_malay_13" src="data/roleplay/Malay/rp1_malay_13.ogg" />
        <File name="rp1_malay_14" src="data/roleplay/Malay/rp1_malay_14.ogg" />
        <File name="rp1_malay_15" src="data/roleplay/Malay/rp1_malay_15.ogg" />
        <File name="rp1_malay_16" src="data/roleplay/Malay/rp1_malay_16.ogg" />
        <File name="rp1_malay_18" src="data/roleplay/Malay/rp1_malay_18.ogg" />
        <File name="rp1_malay_19" src="data/roleplay/Malay/rp1_malay_19.ogg" />
        <File name="rp1_malay_2" src="data/roleplay/Malay/rp1_malay_2.ogg" />
        <File name="rp1_malay_20" src="data/roleplay/Malay/rp1_malay_20.ogg" />
        <File name="rp1_malay_21" src="data/roleplay/Malay/rp1_malay_21.ogg" />
        <File name="rp1_malay_22" src="data/roleplay/Malay/rp1_malay_22.ogg" />
        <File name="rp1_malay_23" src="data/roleplay/Malay/rp1_malay_23.ogg" />
        <File name="rp1_malay_24" src="data/roleplay/Malay/rp1_malay_24.ogg" />
        <File name="rp1_malay_25" src="data/roleplay/Malay/rp1_malay_25.ogg" />
        <File name="rp1_malay_3" src="data/roleplay/Malay/rp1_malay_3.ogg" />
        <File name="rp1_malay_4" src="data/roleplay/Malay/rp1_malay_4.ogg" />
        <File name="rp1_malay_5" src="data/roleplay/Malay/rp1_malay_5.ogg" />
        <File name="rp1_malay_6" src="data/roleplay/Malay/rp1_malay_6.ogg" />
        <File name="rp1_malay_7" src="data/roleplay/Malay/rp1_malay_7.ogg" />
        <File name="rp1_malay_8" src="data/roleplay/Malay/rp1_malay_8.ogg" />
        <File name="rp1_malay_9" src="data/roleplay/Malay/rp1_malay_9.ogg" />
        <File name="roleplay" src="data/roleplay/Tamil/roleplay.json" />
        <File name="rp1_tamil_1" src="data/roleplay/Tamil/rp1_tamil_1.ogg" />
        <File name="rp1_tamil_10" src="data/roleplay/Tamil/rp1_tamil_10.ogg" />
        <File name="rp1_tamil_11" src="data/roleplay/Tamil/rp1_tamil_11.ogg" />
        <File name="rp1_tamil_13" src="data/roleplay/Tamil/rp1_tamil_13.ogg" />
        <File name="rp1_tamil_14" src="data/roleplay/Tamil/rp1_tamil_14.ogg" />
        <File name="rp1_tamil_15" src="data/roleplay/Tamil/rp1_tamil_15.ogg" />
        <File name="rp1_tamil_16" src="data/roleplay/Tamil/rp1_tamil_16.ogg" />
        <File name="rp1_tamil_18" src="data/roleplay/Tamil/rp1_tamil_18.ogg" />
        <File name="rp1_tamil_19" src="data/roleplay/Tamil/rp1_tamil_19.ogg" />
        <File name="rp1_tamil_2" src="data/roleplay/Tamil/rp1_tamil_2.ogg" />
        <File name="rp1_tamil_20" src="data/roleplay/Tamil/rp1_tamil_20.ogg" />
        <File name="rp1_tamil_21" src="data/roleplay/Tamil/rp1_tamil_21.ogg" />
        <File name="rp1_tamil_22" src="data/roleplay/Tamil/rp1_tamil_22.ogg" />
        <File name="rp1_tamil_23" src="data/roleplay/Tamil/rp1_tamil_23.ogg" />
        <File name="rp1_tamil_24" src="data/roleplay/Tamil/rp1_tamil_24.ogg" />
        <File name="rp1_tamil_25" src="data/roleplay/Tamil/rp1_tamil_25.ogg" />
        <File name="rp1_tamil_3" src="data/roleplay/Tamil/rp1_tamil_3.ogg" />
        <File name="rp1_tamil_4" src="data/roleplay/Tamil/rp1_tamil_4.ogg" />
        <File name="rp1_tamil_5" src="data/roleplay/Tamil/rp1_tamil_5.ogg" />
        <File name="rp1_tamil_6" src="data/roleplay/Tamil/rp1_tamil_6.ogg" />
        <File name="rp1_tamil_7" src="data/roleplay/Tamil/rp1_tamil_7.ogg" />
        <File name="rp1_tamil_8" src="data/roleplay/Tamil/rp1_tamil_8.ogg" />
        <File name="rp1_tamil_9" src="data/roleplay/Tamil/rp1_tamil_9.ogg" />
        <File name="localization" src="src/localization.py" />
        <File name="roleplay2" src="data/roleplay/Malay/roleplay2.json" />
        <File name="rp2_malay_1" src="data/roleplay/Malay/rp2_malay_1.ogg" />
        <File name="rp2_malay_10" src="data/roleplay/Malay/rp2_malay_10.ogg" />
        <File name="rp2_malay_11" src="data/roleplay/Malay/rp2_malay_11.ogg" />
        <File name="rp2_malay_12" src="data/roleplay/Malay/rp2_malay_12.ogg" />
        <File name="rp2_malay_13" src="data/roleplay/Malay/rp2_malay_13.ogg" />
        <File name="rp2_malay_14" src="data/roleplay/Malay/rp2_malay_14.ogg" />
        <File name="rp2_malay_15" src="data/roleplay/Malay/rp2_malay_15.ogg" />
        <File name="rp2_malay_16" src="data/roleplay/Malay/rp2_malay_16.ogg" />
        <File name="rp2_malay_17" src="data/roleplay/Malay/rp2_malay_17.ogg" />
        <File name="rp2_malay_18" src="data/roleplay/Malay/rp2_malay_18.ogg" />
        <File name="rp2_malay_19" src="data/roleplay/Malay/rp2_malay_19.ogg" />
        <File name="rp2_malay_2" src="data/roleplay/Malay/rp2_malay_2.ogg" />
        <File name="rp2_malay_20" src="data/roleplay/Malay/rp2_malay_20.ogg" />
        <File name="rp2_malay_21" src="data/roleplay/Malay/rp2_malay_21.ogg" />
        <File name="rp2_malay_22" src="data/roleplay/Malay/rp2_malay_22.ogg" />
        <File name="rp2_malay_23" src="data/roleplay/Malay/rp2_malay_23.ogg" />
        <File name="rp2_malay_24" src="data/roleplay/Malay/rp2_malay_24.ogg" />
        <File name="rp2_malay_25" src="data/roleplay/Malay/rp2_malay_25.ogg" />
        <File name="rp2_malay_3" src="data/roleplay/Malay/rp2_malay_3.ogg" />
        <File name="rp2_malay_4" src="data/roleplay/Malay/rp2_malay_4.ogg" />
        <File name="rp2_malay_5" src="data/roleplay/Malay/rp2_malay_5.ogg" />
        <File name="rp2_malay_6" src="data/roleplay/Malay/rp2_malay_6.ogg" />
        <File name="rp2_malay_7" src="data/roleplay/Malay/rp2_malay_7.ogg" />
        <File name="rp2_malay_8" src="data/roleplay/Malay/rp2_malay_8.ogg" />
        <File name="rp2_malay_9" src="data/roleplay/Malay/rp2_malay_9.ogg" />
        <File name="roleplay2" src="data/roleplay/Tamil/roleplay2.json" />
        <File name="rp2_tamil_1" src="data/roleplay/Tamil/rp2_tamil_1.ogg" />
        <File name="rp2_tamil_10" src="data/roleplay/Tamil/rp2_tamil_10.ogg" />
        <File name="rp2_tamil_11" src="data/roleplay/Tamil/rp2_tamil_11.ogg" />
        <File name="rp2_tamil_12" src="data/roleplay/Tamil/rp2_tamil_12.ogg" />
        <File name="rp2_tamil_13" src="data/roleplay/Tamil/rp2_tamil_13.ogg" />
        <File name="rp2_tamil_14" src="data/roleplay/Tamil/rp2_tamil_14.ogg" />
        <File name="rp2_tamil_15" src="data/roleplay/Tamil/rp2_tamil_15.ogg" />
        <File name="rp2_tamil_16" src="data/roleplay/Tamil/rp2_tamil_16.ogg" />
        <File name="rp2_tamil_17" src="data/roleplay/Tamil/rp2_tamil_17.ogg" />
        <File name="rp2_tamil_18" src="data/roleplay/Tamil/rp2_tamil_18.ogg" />
        <File name="rp2_tamil_19" src="data/roleplay/Tamil/rp2_tamil_19.ogg" />
        <File name="rp2_tamil_2" src="data/roleplay/Tamil/rp2_tamil_2.ogg" />
        <File name="rp2_tamil_20" src="data/roleplay/Tamil/rp2_tamil_20.ogg" />
        <File name="rp2_tamil_21" src="data/roleplay/Tamil/rp2_tamil_21.ogg" />
        <File name="rp2_tamil_22" src="data/roleplay/Tamil/rp2_tamil_22.ogg" />
        <File name="rp2_tamil_23" src="data/roleplay/Tamil/rp2_tamil_23.ogg" />
        <File name="rp2_tamil_24" src="data/roleplay/Tamil/rp2_tamil_24.ogg" />
        <File name="rp2_tamil_25" src="data/roleplay/Tamil/rp2_tamil_25.ogg" />
        <File name="rp2_tamil_3" src="data/roleplay/Tamil/rp2_tamil_3.ogg" />
        <File name="rp2_tamil_4" src="data/roleplay/Tamil/rp2_tamil_4.ogg" />
        <File name="rp2_tamil_5" src="data/roleplay/Tamil/rp2_tamil_5.ogg" />
        <File name="rp2_tamil_6" src="data/roleplay/Tamil/rp2_tamil_6.ogg" />
        <File name="rp2_tamil_7" src="data/roleplay/Tamil/rp2_tamil_7.ogg" />
        <File name="rp2_tamil_8" src="data/roleplay/Tamil/rp2_tamil_8.ogg" />
        <File name="rp2_tamil_9" src="data/roleplay/Tamil/rp2_tamil_9.ogg" />
        <File name="localization" src="src/localization.pyc" />
        <File name="" src="data/memoryGame/.DS_Store" />
        <File name="mem_malay_11_0" src="data/soundTherapy/Malay/mem_malay_11_0.ogg" />
        <File name="mem_malay_11_1" src="data/soundTherapy/Malay/mem_malay_11_1.ogg" />
        <File name="sound_therapy" src="data/soundTherapy/Malay/sound_therapy.json" />
        <File name="st_malay_1" src="data/soundTherapy/Malay/st_malay_1.ogg" />
        <File name="st_malay_2" src="data/soundTherapy/Malay/st_malay_2.ogg" />
        <File name="st_malay_3" src="data/soundTherapy/Malay/st_malay_3.ogg" />
        <File name="st_malay_4" src="data/soundTherapy/Malay/st_malay_4.ogg" />
        <File name="st_malay_5" src="data/soundTherapy/Malay/st_malay_5.ogg" />
        <File name="mem_tamil_11" src="data/soundTherapy/Tamil/mem_tamil_11.ogg" />
        <File name="sound_therapy" src="data/soundTherapy/Tamil/sound_therapy.json" />
        <File name="st_tamil_1" src="data/soundTherapy/Tamil/st_tamil_1.ogg" />
        <File name="st_tamil_2" src="data/soundTherapy/Tamil/st_tamil_2.ogg" />
        <File name="st_tamil_3" src="data/soundTherapy/Tamil/st_tamil_3.ogg" />
        <File name="st_tamil_4" src="data/soundTherapy/Tamil/st_tamil_4.ogg" />
        <File name="st_tamil_5" src="data/soundTherapy/Tamil/st_tamil_5.ogg" />
        <File name="app.7390c7e6" src="html/cms/js/app.7390c7e6.js" />
        <File name="app.7390c7e6.js" src="html/cms/js/app.7390c7e6.js.map" />
        <File name="ex_malay_1" src="data/exercises/Malay/ex_malay_1.ogg" />
        <File name="ex_malay_2" src="data/exercises/Malay/ex_malay_2.ogg" />
        <File name="ex_malay_3" src="data/exercises/Malay/ex_malay_3.ogg" />
        <File name="ex_malay_4_0" src="data/exercises/Malay/ex_malay_4_0.ogg" />
        <File name="ex_malay_4_1" src="data/exercises/Malay/ex_malay_4_1.ogg" />
        <File name="ex_malay_4_2" src="data/exercises/Malay/ex_malay_4_2.ogg" />
        <File name="ex_malay_4_3" src="data/exercises/Malay/ex_malay_4_3.ogg" />
        <File name="ex_malay_4_4" src="data/exercises/Malay/ex_malay_4_4.ogg" />
        <File name="ex_malay_5" src="data/exercises/Malay/ex_malay_5.ogg" />
        <File name="ex_malay_6_0" src="data/exercises/Malay/ex_malay_6_0.ogg" />
        <File name="ex_malay_6_1" src="data/exercises/Malay/ex_malay_6_1.ogg" />
        <File name="exercises" src="data/exercises/Malay/exercises.json" />
        <File name="ex_tamil_1" src="data/exercises/Tamil/ex_tamil_1.ogg" />
        <File name="ex_tamil_2" src="data/exercises/Tamil/ex_tamil_2.ogg" />
        <File name="ex_tamil_3" src="data/exercises/Tamil/ex_tamil_3.ogg" />
        <File name="ex_tamil_4_0" src="data/exercises/Tamil/ex_tamil_4_0.ogg" />
        <File name="ex_tamil_4_1" src="data/exercises/Tamil/ex_tamil_4_1.ogg" />
        <File name="ex_tamil_4_2" src="data/exercises/Tamil/ex_tamil_4_2.ogg" />
        <File name="ex_tamil_4_3" src="data/exercises/Tamil/ex_tamil_4_3.ogg" />
        <File name="ex_tamil_4_4" src="data/exercises/Tamil/ex_tamil_4_4.ogg" />
        <File name="ex_tamil_5" src="data/exercises/Tamil/ex_tamil_5.ogg" />
        <File name="ex_tamil_6_0" src="data/exercises/Tamil/ex_tamil_6_0.ogg" />
        <File name="ex_tamil_6_1" src="data/exercises/Tamil/ex_tamil_6_1.ogg" />
        <File name="exercises" src="data/exercises/Tamil/exercises.json" />
    </Resources>
    <Topics />
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
        <Translation name="translation_ja_JP" src="translations/translation_ja_JP.ts" language="ja_JP" />
        <Translation name="translation_zh_CN" src="translations/translation_zh_CN.ts" language="zh_CN" />
    </Translations>
</Package>
