#!/usr/bin/env python

# Copyright 2018 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Google Cloud Text-To-Speech API sample application .

Example usage:
  python synthesize_text.py --text "hello"
  python synthesize_text.py --ssml "<speak>Hello there.</speak>"
"""

import argparse
import os
from unicodedata import name
from google.cloud import texttospeech


def synthesize_speech(text, language, isSSML=False, output="output.mp3"):
  client = texttospeech.TextToSpeechClient()
  if isSSML:
    input_text = texttospeech.SynthesisInput(ssml=text)
  else:
    input_text = texttospeech.SynthesisInput(text=text)
  # Note: the voice can also be specified by name.
  # Names of voices can be retrieved with client.list_voices().
  if language == "Tamil":
    voice = texttospeech.VoiceSelectionParams(
      language_code="ta-IN",
      name="ta-IN-Wavenet-A",
      ssml_gender=texttospeech.SsmlVoiceGender.FEMALE,
    )
  elif language == "Malay":
    voice = texttospeech.VoiceSelectionParams(
      language_code="ms-MY",
      name="ms-MY-Standard-A",
      ssml_gender=texttospeech.SsmlVoiceGender.FEMALE,
    )

  audio_config = texttospeech.AudioConfig(
    audio_encoding=texttospeech.AudioEncoding.MP3
  )

  print(input_text)
  response = client.synthesize_speech(
    input=input_text, voice=voice, audio_config=audio_config
  )
  # The response's audio_content is binary.
  if not os.path.exists("dist/"):
    os.makedirs("dist")
  with open("dist/"+output, "wb") as out:
    out.write(response.audio_content)
    print('Audio content written to file "' + output + '"')


if __name__ == "__main__":
  text = "Bolehkah anda tolong saya mengenal pasti mereka? Mari kita mulakan!"
  text = '<speak><prosody rate="75%">' + text + '</prosody></speak>'
  output = "QZ_Malay_3.mp3"
  synthesize_speech(text, "Malay", isSSML=True, output=output)

# if __name__ == "__main__":
#   parser = argparse.ArgumentParser(
#     description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
#   )
#   group = parser.add_mutually_exclusive_group(required=True)
#   group.add_argument("--text", help="The text from which to synthesize speech.")
#   group.add_argument(
#     "--ssml", help="The ssml string from which to synthesize speech."
#   )
#   parser.add_argument("--out", help="output filename")

#   args = parser.parse_args()

#   if args.text:
#     text = args.text
#     isSSML = False
#   else:   
#     text = args.ssml
#     isSSML = True
#   if args.out:
#     out = args.out
#   else:
#     out = "output.mp3"
#   synthesize_speech(text, "English", isSSML=isSSML, output=out)
