module.exports = {
  lintOnSave: true,
  publicPath: process.env.VUE_APP_PATH || '/apps/cgh-main/cms/',
  outputDir: process.env.VUE_APP_BUILD_DIR || '../cgh-package/html/cms'
}