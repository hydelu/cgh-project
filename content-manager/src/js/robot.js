export const robot = {
  session: null,
  isConnected: false,
  debugLog (msg) {
    if (robot.debug) {
      console.log(msg)
    }
  },
  connect (ip, onSuccess, onFail) {
    if (ip != undefined) {
      this.session = new QiSession(ip)
    } else {
      this.session = new QiSession()
    }
    var ref = this
    this.session.socket().on('connect', function () {
      robot.debugLog("Qimessaging: connected!")
      ref.isConnected = true
      if (onSuccess != undefined) {
        setTimeout(onSuccess, 100)
      }
    })
    this.session.socket().on('disconnect', function() {
      robot.debugLog('Qimessaging: disconnected!');
      ref.isConnected = false
      if (onFail != undefined) {
        setTimeout(onFail, 100)
      }
    });
  },
  getService (serviceName) {
    return this.session.service(serviceName)
  },
  call (serviceName, functionName, callback, errorCallback, ...params) {
    this.getService(serviceName).done(function (service) {
      //console.log("qicli call: " + serviceName + "." + functionName)
      try {
        service[functionName](...params).done(callback).fail(errorCallback)
      } catch (e) {
        errorCallback(e)
      }
    })
  },
  setLanguage (lang, callback) {
    this.call("ALTextToSpeech", "setLanguage", () => {
      this.call("ALSpeechRecognition", "setLanguage", callback, null, lang)
    }, null, lang)
  },
  startActivity(template, config) {
    this.call("CGHTemplateController", "start_template", null, null, template, config)
  },
  subscribeToTabletCommands(callback) {
    if (!this.isConnected) {
      setTimeout(() => { this.subscribeToTabletCommands(callback) }, 500)
    } else {
      this.subscribeToSignal("CGHTemplateController", "showOnTablet", callback)
    }
  },
  subscribeToSignal(serviceName, signalName, callback) {
    this.getService(serviceName).done(function (service) {
      try {
        service[signalName].connect(callback)
      } catch (e) {
        console.error(e)
      }
    })
  },
  uploadBackupToPath(path, serverIp, file, onSuccess, onFail) {
    let formData = new FormData()
    formData.append(file.name, file)
    let url = "http://" + serverIp
    fetch(url, {
      method: "POST",
      body: formData
    })
      .then((response) => {
        response.text().then((text) => {
          this.call(
            "CGHManager",
            "install_backup",
            onSuccess,
            onFail,
            text,
            path
          )
        })
      })
      .catch(onFail)
  },
  downloadBackupFromPath(path, serverIp, backupName, onSuccess, onFail) {
    this.call(
      "CGHManager",
      "create_backup",
      (data) => {
        this.downloadFile(data, serverIp, backupName, onSuccess, onFail)
      },
      onFail, path)
  },
  downloadFile (path, serverIp, backupName, onSuccess, onFail) {
    let url = "http://" + serverIp + path
    fetch(url, {
      method: "GET"
    })
      .then((response) => response.blob())
      .then((blob) => {
        let blobUrl = window.URL.createObjectURL(blob)
        let a = document.createElement("a")
        a.href = blobUrl
        a.download = backupName
        document.body.appendChild(a) // we need to append the element to the dom -> otherwise it will not work in firefox
        a.click()
        a.remove() //afterwards we remove the element again
      })
      .catch((err) => {
        onFail(err)
      })
  }
}