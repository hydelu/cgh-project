import Vue from 'vue'

export const store = {
  state: Vue.observable({
    wifiIp: "192.168.11.2",
    currentState: "home",
    currentPresetId: "",
    currentSound: null,
    currentSong: null,
    currentQuestion: null,
    currentSequence:null,
    currentPreset: null,
    currentLanguage: "English",
    currentCategory: null
  }),
  possibleStates: ["home", "realityOrientation",
    "soundTherapy", "stPresetEdit", "stMusic", "stMusicEdit",
    "exercises", "exerciseEdit", "exerciseMusic",
    "themeSong", "tsEdit",
    "quiz", "qzEdit", "qzList","qzQuestionEdit",
    "memoryGame", "mgPresetEdit", "mgCategories", "mgCategoryEdit",
    "categories", "catPresetEdit", "catLocations", "catLocationEdit",
    "roleplayChoice", "roleplay1", "roleplay2", "roleplay1Edit", "roleplay2Edit",
    "similarityGame","sgPresetEdit","sgSequenceList","sgSequenceEdit"

  ],
  setState(state) {
    if (this.possibleStates.indexOf(state) != -1)
      this.state.currentState = state
    else
      throw 'impossible state: ' + state
  },
  setPresetId(presetId) {
    this.state.currentPresetId = presetId
  },
  setCurrentSound(sound) {
    Vue.set(this.state, "currentSound", sound)
  },
  setCurrentCategory(cat) {
    Vue.set(this.state, "currentCategory", cat)
  },
  setCurrentLanguage(language) {
    this.state.currentLanguage = language
  },
  setCurrentSong(song) {
    Vue.set(this.state, "currentSong", song)
  },
  setSongData(data) {
    this.state.songData = data
  },
  setCurrentQuestion(question) {
    Vue.set(this.state, "currentQuestion", question)
  },
  setCurrentPreset(preset) {
    Vue.set(this.state, "currentPreset", preset)
  },
  setCurrentSequence(sequence) {
    Vue.set(this.state, "currentSequence", sequence)
  },
}